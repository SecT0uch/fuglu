#!/bin/bash

# run in fuglu repository root directory

cd fuglu/src
FUGLUVERSION=$(python3 -c 'from fuglu import __version__; print(__version__)')
cd ..

git tag -a $FUGLUVERSION -m "tag $FUGLUVERSION"
git push --tags origin master
git push --tags upstream master

python3 setup.py sdist
twine upload dist/fuglu-$FUGLUVERSION.tar.gz
cd ..