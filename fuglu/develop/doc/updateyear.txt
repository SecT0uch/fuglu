#########################
# UPDATE LICENSE NOTICE #
#########################

find . \( -type d -name .git -prune \) -o -type f -print0 | xargs -0 sed -i 's/-2019 /-2020 /'

