# -*- coding: UTF-8 -*-
#   Copyright 2009-2021 Oli Schacher, Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
__all__ = ['archive', 'attachment', 'decision', 'clamav', 'sa', 'p_debug', 'p_blwl', 'a_logging', 'bacn',
           'p_skipper', 'p_fraction', 'vacation', 'antivirus', 'sssp', 'domainauth', 'script', 'a_statsd',
           'fuzor', 'rspamd', 'mailcopy', 'uriextract', 'call_ahead', 'delay', 'ivmsg', 'domainauth',
           'tlspolicy', 'originpolicy', 'outpolicy', 'restrictions', ]
