[main]

#identifier can be any string that helps you identifying your config file
#this helps making sure the correct config is loaded. this identifier will be printed out when fuglu is reloading its config
identifier=dist

#run as a daemon? (fork)
daemonize=1

#Enable session scantime logger
scantimelogger=0

#run as user
user=nobody

#run as group
group=nobody

#comma separated list of directories in which fuglu searches for additional plugins and their dependencies
plugindir=

#what SCANNER plugins do we load, comma separated
plugins=archive,attachment,clamav,spamassassin

#what PREPENDER plugins do we load, comma separated
prependers=debug,skip

#what APPENDER plugins do we load, comma separated
#appender plugins are plugins run after the scanning plugins
#appenders will always be run, even if a a scanner plugin decided to delete/bounce/whatever a message
#(unless a mail is deferred in which case running the appender would not make sense as it will come again)
appenders=

#address fuglu should listen on. usually 127.0.0.1 so connections are accepted from local host only
bindaddress=127.0.0.1

#incoming port(s) (postfix connects here)
#you can use multiple comma separated ports here
#f.ex. to separate incoming and outgoing mail and a special port for debugging messages
#10025: standard incoming mail
#10099: outgoing mail
#10888: debug port
incomingport=10025,10099,10888

#outgoing hostname/ip where postfix is listening for re-injects.
#use ${injecthost} to connect back to the IP where the incoming connection came from
outgoinghost=127.0.0.1

#outgoing port  where postfix is listening for re-injects)
outgoingport=10026

##outgoing helo we should use for re-injects
#leave empty to auto-detect current hostname
outgoinghelo=

#temp dir where fuglu can store messages while scanning
tempdir=/tmp

#String to prepend to added headers
prependaddedheaders=X-Fuglu-

#If a plugin decides to delete a message, save a copy here
#default empty, eg. do not save a backup copy
trashdir=

#list all deleted messages in 00-fuglutrash.log in the trashdir
trashlog=0

#if this is set to True/1/yes , no Bounces will be sent from Fuglu eg. after a blocked attachment has been detected
#This may be used for debugging/testing to make sure fuglu can not produce backscatter
disablebounces=0

#list of domains to which no bounces will be sent
nobouncefile=${confdir}/rules/nobounce.txt

#write debug info header to every mail
debuginfoheader=0

#write a Spamstatus YES/NO header
spamstatusheader=1

#write suspect ID to every mail
suspectidheader=1

#write mrtg statistics
mrtgdir=

#port where fuglu provides statistics etc (used by fuglu_control). Can also be a path to a unix socket
controlport=/tmp/fuglu_control.sock

#Log pattern to use for all suspects in fuglu log. set empty string to disable logging generic suspect info. Supports the usual template variables plus: ${size}, ${spam} ${highspam}, ${modified} ${decision} ${tags} (short tags representagion) ${fulltags} full tags output, ${decision}
logtemplate=Suspect ${id} from=${from_address} to=${to_address} size=${size} spam=${spam} virus=${virus} modified=${modified} decision=${decision}

#warn about known severe problems/security issues of current version.
#Note: This performs a DNS lookup of gitrelease.patchlevel.minorversion.majorversion.versioncheck.fuglu.org on startup and fuglu --lint.
#No other information of any kind is transmitted to outside systems.
#Disable this if you consider the DNS lookup an unwanted information leak.
versioncheck=1

#Method to check mail address validity ("Default","LazyLocalPart")
address_compliance_checker=Default

#Action to perform if address validity check fails ("defer","reject","discard")
address_compliance_fail_action=defer

#Reply message if address validity check fails
address_compliance_fail_message=invalid sender or recipient address

#Remove temporary message file from disk for receive or address compliance errors
remove_tmpfiles_on_error=1

[PluginAlias]

rspamd=fuglu.plugins.rspamd.RSpamdPlugin

fuzorcheck=fuglu.plugins.fuzor.FuzorCheck

fuzorreport=fuglu.plugins.fuzor.FuzorReport

a_fuzorreport=fuglu.plugins.fuzor.FuzorReportAppender

salearn=fuglu.plugins.sa.SALearn

dmarc=fuglu.plugins.domainauth.DMARCPlugin

uriextract=fuglu.plugins.uriextract.URIExtract

a_uriextract=fuglu.plugins.uriextract.URIExtractAppender

domainaction=fuglu.plugins.uriextract.DomainAction

emailextract=fuglu.plugins.uriextract.EmailExtract

a_emailextract=fuglu.plugins.uriextract.EmailExtractAppender

mailaction=fuglu.plugins.uriextract.MailAction

debug=fuglu.plugins.p_debug.MessageDebugger

skip=fuglu.plugins.p_skipper.PluginSkipper

fraction=fuglu.plugins.p_fraction.PluginFraction

archive=fuglu.plugins.archive.ArchivePlugin

attachment=fuglu.plugins.attachment.FiletypePlugin

clamav=fuglu.plugins.clamav.ClamavPlugin

spamassassin=fuglu.plugins.sa.SAPlugin

vacation=fuglu.plugins.vacation.VacationPlugin

actionoverride=fuglu.plugins.decision.ActionOverridePlugin

killer=fuglu.plugins.decision.KillerPlugin

icap=fuglu.plugins.antivirus.ICAPPlugin

sssp=fuglu.plugins.sssp.SSSPPlugin

fprot=fuglu.plugins.antivirus.FprotPlugin

scriptfilter=fuglu.plugins.script.ScriptFilter

dkimsign=fuglu.plugins.domainauth.DKIMSignPlugin

dkimverify=fuglu.plugins.domainauth.DKIMVerifyPlugin

spf=fuglu.plugins.domainauth.SPFPlugin

[performance]

#minimum scanner threads
minthreads=2

#maximum scanner threads
maxthreads=40

#minimum free scanner threads
minfreethreads=0

#Method for parallelism, either 'thread' or 'process' 
backend=thread

#Initial number of processes when backend='process'. If 0 (the default), automatically selects twice the number of available virtual cores. Despite its 'initial'-name, this number currently is not adapted automatically.
initialprocs=0

#Maximum cache size to keep attachemnts (archives extracted) per suspect during mail analysis (in bytes, default: 50MB)
att_mgr_cachesize=50000000

#Default maximum filesize to extract from archives (in bytes, default: 50MB)
att_mgr_default_maxextract=50000000

#Upper maximum filesize limit to extract from archives (in bytes, default: 500MB)
att_mgr_hard_maxextract=500000000

#Default limit for maximum number of files to be extracted from archives (default: 500)
att_mgr_default_maxnfiles=500

#Upper limit for maximum number of files to be extracted from archives (default: 500)
att_mgr_hard_maxnfiles=500

#do not use aiosmtplib even if it is installed (fallback to python built in smtplib)
disable_aiosmtp=False

[spam]

#what to do with messages that plugins think are spam but  not so sure  ("low spam")
#in normal usage you probably never set this something other than DUNNO
#this is a DEFAULT action, eg. anti spam plugins should take this if you didn't set 
# a individual override
defaultlowspamaction=DUNNO

#what to do with messages if a plugin is sure it is spam ("high spam") 
#in after-queue mode this is probably still DUNNO or maybe DELETE for courageous people
#this is a DEFAULT action, eg. anti spam plugins should take this if you didn't set
# a individual override 
defaulthighspamaction=DUNNO

[virus]

##what to do with messages if a plugin detects a virus
#in after-queue mode this should probably be DELETE
#in pre-queue mode you could use REJECT
#this is a DEFAULT action, eg. anti-virus plugins should take this if you didn't set 
# a individual override
defaultvirusaction=DELETE

[smtpconnector]

#confirmation template sent back to the connecting postfix for accepted messages
requeuetemplate=FUGLU REQUEUE(${id}): ${injectanswer}

[esmtpconnector]

#confirmation template sent back to the connecting client for accepted messages
queuetemplate=${injectanswer}

#only deliver the message to the first recipient, ignore the others. This is useful in spamtrap setups where we don't want to create duplicate deliveries.
ignore_multiple_recipients=0

[databaseconfig]

#read runtime configuration values from a database. requires sqlalchemy to be installed
dbconnectstring=

#sql query that returns a configuration value override. sql placeholders are ':section',':option' in addition the usual suspect filter default values like ':to_domain', ':to_address' etc
#if the statement returns more than one row/value only the first value in the first row is used
sql=SELECT value FROM fugluconfig WHERE `section`=:section AND `scope` IN ('$GLOBAL',CONCAT('%',:to_domain),:to_address) AND `option`=:option ORDER BY `scope` DESC LIMIT 1

#sql query that returns all values, options and scopes (relevant for one recipient) of one section. sql placeholders ':section' available in addition the usual suspect filter default values like ':to_domain', ':to_address' etc
sqlsection=SELECT value, option, scope FROM fugluconfig WHERE `section`=:section AND `scope` IN ('$GLOBAL',CONCAT('%',:to_domain),:to_address) ORDER BY `scope` DESC

[environment]

#Distance to the boundary MTA ("how many received headers should fuglu skip to determine the last untrusted host information"). Only required if plugins need to have information about the last untrusted host(SPFPlugin)
boundarydistance=0

#Optional regex that should be applied to received headers to skip trusted (local) mta helo/ip/reverse dns.
#Only required if plugins need to have information about the last untrusted host and the message doesn't pass a fixed amount of hops to reach this system in your network
trustedhostsregex=

#Optional regex that should be applied to received headers to skip trusted (local) mta transfers (for example LMTP).
#Only required if plugins need to have information about the last untrusted host and the message doesn't pass a fixed amount of hops to reach this system in your network
trustedreceivedregex=

[ArchivePlugin]

#storage for archived messages
local_archivedir=/tmp

#subdirectory within archivedir
local_subdirtemplate=${to_domain}/${to_localpart}/${date}

#filename template for the archived messages
local_filenametemplate=${archiveid}.eml

#if true/1/yes: store original message
#if false/0/no: store message probably altered by previous plugins, eg with spamassassin headers
local_useoriginal=True

#change owner of saved messages (username or numeric id) - this only works if fuglu is running as root (which is NOT recommended)
local_chown=

#change group of saved messages (groupname or numeric id) - the user running fuglu must be a member of the target group for this to work
local_chgrp=

#set file permissions of saved messages
local_chmod=

#comma separated list of LMTP target hostname, hostname:port or path to local LMTP socket (path must start with /)
lmtp_hosts=

#LMTP auth user. leave empty if no authentication is needed
lmtp_user=

#LMTP auth password. leave empty if no authentication is needed
lmtp_password=

#LMTP envelope sender. Leave empty for original SMTP envelope sender
lmtp_sender=

#should we store the original message as retreived from postfix or store the
#                                current state in fuglu (which might have been altered by previous plugins)
lmtp_useoriginal=True

#Name of header containing Fuglu ID when storing via LMTP
lmtp_headername=X-Fuglu-ID

#comma separated list of ElasticSearch host definition (hostname, hostname:port, https://user:pass@hostname:port/)
elastic_uris=

#Name of ElasticSearch index in which document will be stored. Template vars (e.g. ${to_domain} or ${date}) can be used.
elastic_index=fugluquar-${date}

#comma separated list of additional fields to be added to document. Any fuglu Suspect variable is permitted (e.g. to_address)
elastic_extrafields=

#should we store the original message as retreived from postfix or store the
#                                current state in fuglu (which might have been altered by previous plugins)
elastic_useoriginal=True

#quarantine cassandra hostnames, separated by comma
cassandra_hosts=

#quarantine cassandra keyspace
cassandra_keyspace=fugluquar

#ttl for quarantined files in seconds
cassandra_ttl=1209600

#should we store the original message as retreived from postfix or store the
#                                current state in fuglu (which might have been altered by previous plugins)
cassandra_useoriginal=True

#Archiving SuspectFilter File
archiverules=${confdir}/archive.regex

#comma separated list of backends to use. available backends: localdir, lmtp, elastic, cassandra
archivebackends=localdir

#set to True to store mail in all enabled backends. set to False to only use primary and fallback to other backends on error
multibackend=False

#Name of header containing alternative Fuglu ID that overrides storage key
fugluid_headername=

#skip archiving if fugluid_headername is not set
fugluid_headername_skipmissing=True

#action if there is a problem (DUNNO, DEFER)
problemaction=DEFER

[FiletypePlugin]

#Mail template for the bounce to inform sender about blocked attachment
template_blockedfile=${confdir}/templates/blockedfile.tmpl

#inform the sender about blocked attachments.
#If a previous plugin tagged the message as spam or infected, no bounce will be sent to prevent backscatter
sendbounce=True

#directory that contains attachment rules
rulesdir=${confdir}/rules

#what should the plugin do when a blocked attachment is detected
#REJECT : reject the message (recommended in pre-queue mode)
#DELETE : discard messages
#DUNNO  : mark as blocked but continue anyway (eg. if you have a later quarantine plugin)
blockaction=DELETE

#sqlalchemy connectstring to load rules from a database and use files only as fallback. requires SQL extension to be enabled
dbconnectstring=

#sql query to load rules from a db. #:scope will be replaced by the recipient address first, then by the recipient domain
#:check will be replaced 'filename','contenttype','archive-filename' or 'archive-contenttype'
query=SELECT action,regex,description FROM attachmentrules WHERE scope=:scope AND checktype=:checktype ORDER BY prio

#enable scanning of filenames within archives (zip,rar). This does not actually extract the files, it just looks at the filenames found in the archive.
checkarchivenames=False

#extract compressed archives(zip,rar) and check file content type with libmagics
#note that the files will be extracted into memory - tune archivecontentmaxsize  accordingly.
#fuglu does not extract archives within the archive(recursion)
checkarchivecontent=False

#only extract and examine files up to this amount of (uncompressed) bytes
archivecontentmaxsize=5000000

#recursive extraction level for archives. Undefined or negative value means extract until it's not an archive anymore
archiveextractlevel=1

#comma separated list of archive extensions. do only process archives of given types.
enabledarchivetypes=

[ClamavPlugin]

#hostname where clamd runs
host=localhost

#tcp port number or path to clamd.sock for unix domain sockets
#example /var/lib/clamav/clamd.sock or on ubuntu: /var/run/clamav/clamd.ctl 
port=3310

#socket timeout
timeout=30

#*EXPERIMENTAL*: Perform multiple scans over the same connection. May improve performance on busy systems.
pipelining=False

#maximum message size, larger messages will not be scanned.  
#should match the 'StreamMaxLength' config option in clamd.conf 
maxsize=22000000

#how often should fuglu retry the connection before giving up
retries=3

#action if infection is detected (DUNNO, REJECT, DELETE)
virusaction=DEFAULTVIRUSACTION

#action if there is a problem (DUNNO, DEFER)
problemaction=DEFER

#reject message template if running in pre-queue mode and virusaction=REJECT
rejectmessage=threat detected: ${virusname}

#*EXPERIMENTAL*: fallback to clamscan if clamd is unavailable. YMMV, each scan can take 5-20 seconds and massively increase load on a busy system.
clamscanfallback=False

#the path to clamscan executable
clamscan=/usr/bin/clamscan

#process timeout
clamscantimeout=30

#define AVScanner engine names causing current plugin to skip if they found already a virus
skip_on_previous_virus=none

#path to file with signature names that can be skipped if message is welcomelisted. list one signature per line, signature is case sensitive
skiplist_file=

[SAPlugin]

#hostname where spamd runs
host=localhost

#tcp port number or path to spamd unix socket
port=783

#how long should we wait for an answer from sa
timeout=30

#maximum size in bytes. larger messages will be skipped
maxsize=256000

#enable scanning of messages larger than maxsize. all attachments will be stripped and only headers, plaintext and html part will be scanned. If message is still oversize it will be truncated.
strip_oversize=1

#how often should fuglu retry the connection before giving up
retries=3

#how long should fuglu wait in seconds before retryng the connection
retry_sleep=1

#should we scan the original message as retreived from postfix or scan the current state 
#in fuglu (which might have been altered by previous plugins)
#only set this to disabled if you have a custom plugin that adds special headers to the message that will be 
#used in spamassassin rules
scanoriginal=True

#forward the original message or replace the content as returned by spamassassin
#if this is enabled, no spamassassin headers will be visible in the final message.
#"original" in this case means "as passed to spamassassin", eg. if 'scanoriginal' above is disabled this will forward the
#message as retreived from previous plugins 
forwardoriginal=False

#what header does SA set to indicate the spam status
#Note that fuglu requires a standard header template configuration for spamstatus and score extraction
#if 'forwardoriginal' is set to 0
#eg. start with _YESNO_ or _YESNOCAPS_ and contain score=_SCORE_
spamheader=X-Spam-Status

#tells fuglu what spamassassin prepends to its headers. Set this according to your spamassassin config especially if you forwardoriginal=0 and strip_oversize=1
spamheader_prepend=X-Spam-

#enable user_prefs in SA. This hands the recipient address over the spamd connection which allows SA to search for configuration overrides
peruserconfig=True

#lowercase user (envelope rcpt) before passing it to spamd
lowercase_user=True

#spamscore threshold to mark a message as high spam
highspamlevel=15

#what should we do with high spam (spam score above highspamlevel)
highspamaction=DEFAULTHIGHSPAMACTION

#what should we do with low spam (eg. detected as spam, but score not over highspamlevel)
lowspamaction=DEFAULTLOWSPAMACTION

#action if there is a problem (DUNNO, DEFER)
problemaction=DEFER

#reject message template if running in pre-queue mode
rejectmessage=message identified as spam

#sqlalchemy db connect string, e.g. mysql:///localhost/spamassassin
sql_blocklist_dbconnectstring=

#Suspect tags to attach as text part to message for scanning
attach_suspect_tags=

[debug]

#messages incoming on this port will be debugged to a logfile
#Make sure the debugport is also set in the incomingport configuration option in the main section
debugport=10888

#debug log output
debugfile=/tmp/fuglu_debug.log

#debugged message can not be bounced
nobounce=True

#don't re-inject debugged messages back to postfix
noreinject=True

#don't run appender plugins for debugged messages
noappender=True

[PluginSkipper]

#path to file containing scanner plugin skip regex rules
filterfile=${confdir}/skipplugins.regex
