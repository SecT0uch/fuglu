# -*- coding: UTF-8 -*-
import typing as tp
import unittest

import fuglu.connectors.milterconnector as sm
import fuglu.connectors.asyncmilterconnector as asm

from fuglu.mshared import BMPConnectMixin, BasicMilterPlugin
from fuglu.connectors.milterconnector import CONTINUE
from fuglu.shared import FuConfigParser

class MPlug(BMPConnectMixin, BasicMilterPlugin):
    def examine_connect(self, sess: tp.Union[sm.MilterSession, asm.MilterSession], host: str, addr: str) -> tp.Union[bytes, tp.Tuple[bytes, str]]:
        return CONTINUE


class TestMilterPluginBase(unittest.TestCase):
    def test_lint_ok(self):
        config = FuConfigParser()

        config.add_section('MPlug')
        config.set('MPlug', 'state', 'connect')

        cand = MPlug(config=config, section='MPlug')
        print(cand.lint())

    def test_lint_notimpl(self):
        config = FuConfigParser()

        config.add_section('MPlug')
        config.set('MPlug', 'state', 'helo')

        cand = MPlug(config=config, section='MPlug')
        print(cand.lint())
