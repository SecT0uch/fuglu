# -*- coding: UTF-8 -*-
import unittest
import yaml
import os
import sys
import libmilter as lm
from configparser import ConfigParser
from unittest.mock import patch, MagicMock

# PyCharm issue (not loading path correctly, only in debug mode)
UNITTESTDIR = os.path.dirname(os.path.realpath(__file__))
if UNITTESTDIR not in sys.path:
    sys.path.insert(0, UNITTESTDIR)

from .unittestsetup import TESTDATADIR

from domainmagic.dnslookup import DNSLookupResult
from fuglu.plugins.restrictions import (
    AccessRestrictions,
    RegexRestriction,
    RestrictionSet,
    RBLRestriction,
    CreateSubdomainListMixin,
)
from fuglu.shared import Suspect
from fuglu.stringencode import force_uString
import fuglu.connectors.milterconnector as sm
import fuglu.connectors.asyncmilterconnector as asm


class TestRBLRestriction(unittest.TestCase):
    def test_hit(self):
        rbldict = yaml.safe_load("""
providertype: "uri-bitmask"
searchdomain: "rbl.domain.invalid"
resultconfig:
    - "2:rbl.domain.invalid"
    - "16:rbl.domain.invalid"
""")
        rbl = RBLRestriction(providertype=rbldict["providertype"],
                             searchdomain=rbldict["searchdomain"],
                             resultconfig=rbldict["resultconfig"]
                             )

        dnsres = DNSLookupResult()
        dnsres.content='127.0.0.2'
        dnsres.qtype='A'
        dnsres.question="hit.domain.invalid.rbl.domain.invalid"
        dnsres.rtype='A'
        dnsres.ttl=2627

        with patch.object(rbl.rbllookup.providers[0].resolver, 'lookup', return_value=[dnsres]):
            is_match = rbl.match("hit.domain.invalid")
        print(is_match)
        self.assertEqual("hit.domain.invalid is listed on rbl.domain.invalid (rbl.domain.invalid)", is_match)

    def test_hit_message(self):
        rbldict = yaml.safe_load("""
        providertype: "uri-bitmask"
        searchdomain: "rbl.domain.invalid"
        message: "CustomizeMessage(RBLRestriction):${output}"
        resultconfig:
            - "2:rbl.domain.invalid"
            - "16:rbl.domain.invalid"
        """)
        rbl = RBLRestriction(providertype=rbldict["providertype"],
                             searchdomain=rbldict["searchdomain"],
                             resultconfig=rbldict["resultconfig"],
                             message=rbldict['message']
                             )

        dnsres = DNSLookupResult()
        dnsres.content = '127.0.0.2'
        dnsres.qtype = 'A'
        dnsres.question = "hit.domain.invalid.rbl.domain.invalid"
        dnsres.rtype = 'A'
        dnsres.ttl = 2627

        with patch.object(rbl.rbllookup.providers[0].resolver, 'lookup', return_value=[dnsres]):
            is_match = rbl.match("hit.domain.invalid")
            examine = rbl.examine("hit.domain.invalid")
        print(is_match)
        print(examine)
        self.assertEqual("CustomizeMessage(RBLRestriction):hit.domain.invalid is listed on rbl.domain.invalid (rbl.domain.invalid)", is_match)
        self.assertEqual("CustomizeMessage(RBLRestriction):hit.domain.invalid is listed on rbl.domain.invalid (rbl.domain.invalid)", examine[1])

    def test_hit_mailhash_defmessage(self):
        """Test hit and default message"""
        rbldict = yaml.safe_load("""
providertype: "email-bitmask"
searchdomain: "mailhash.domain.invalid"
resultconfig:
 - "2:mailhash.domain.invalid"
message:
""")
        rbl = RBLRestriction(providertype=rbldict["providertype"],
                             searchdomain=rbldict["searchdomain"],
                             resultconfig=rbldict["resultconfig"],
                             message=rbldict["message"]
                             )

        dnsres = DNSLookupResult()
        dnsres.content='127.0.0.2'
        dnsres.name=None
        dnsres.qtype='A'
        dnsres.question="806c1b74300a6a3674e67f3f0425f2f3.mailhash.domain.invalid"
        dnsres.rtype='A'
        dnsres.ttl=1

        with patch.object(rbl.rbllookup.providers[0].resolver, 'lookup', return_value=[dnsres]):
            is_match = rbl.match("aaaaaaaaaaaaaaaaaa@aaaaaaa.aaa")
        print(is_match)
        self.assertEqual("aaaaaaaaaaaaaaaaaa@aaaaaaa.aaa is listed on mailhash.domain.invalid (mailhash.domain.invalid)", is_match)

    def test_hit_mailhash_output(self):
        """Test hit and message using output keyword"""
        rbldict = yaml.safe_load("""
providertype: "email-bitmask"
searchdomain: "mailhash.domain.invalid"
resultconfig:
 - "2:mailhash.domain.invalid"
message: "rbl output: '${output}' here"
""")
        rbl = RBLRestriction(providertype=rbldict["providertype"],
                             searchdomain=rbldict["searchdomain"],
                             resultconfig=rbldict["resultconfig"],
                             message=rbldict["message"]
                             )

        dnsres = DNSLookupResult()
        dnsres.content='127.0.0.2'
        dnsres.name=None
        dnsres.qtype='A'
        dnsres.question="806c1b74300a6a3674e67f3f0425f2f3.mailhash.domain.invalid"
        dnsres.rtype='A'
        dnsres.ttl=1

        with patch.object(rbl.rbllookup.providers[0].resolver, 'lookup', return_value=[dnsres]):
            is_match = rbl.match("aaaaaaaaaaaaaaaaaa@aaaaaaa.aaa")
        print(is_match)
        self.assertEqual("rbl output: 'aaaaaaaaaaaaaaaaaa@aaaaaaa.aaa is listed on mailhash.domain.invalid (mailhash.domain.invalid)' here", is_match)

    def test_hit_mailhash_custom(self):
        """Test hit and message using basic keywords"""
        rbldict = yaml.safe_load("""
providertype: "email-bitmask"
searchdomain: "mailhash.domain.invalid"
resultconfig:
 - "2:mailhash.domain.invalid"
message: "${input} is listed on ${rbldomain}"
""")
        rbl = RBLRestriction(providertype=rbldict["providertype"],
                             searchdomain=rbldict["searchdomain"],
                             resultconfig=rbldict["resultconfig"],
                             message=rbldict["message"]
                             )

        dnsres = DNSLookupResult()
        dnsres.content='127.0.0.2'
        dnsres.name=None
        dnsres.qtype='A'
        dnsres.question="806c1b74300a6a3674e67f3f0425f2f3.mailhash.domain.invalid"
        dnsres.rtype='A'
        dnsres.ttl=1

        with patch.object(rbl.rbllookup.providers[0].resolver, 'lookup', return_value=[dnsres]):
            is_match = rbl.match("aaaaaaaaaaaaaaaaaa@aaaaaaa.aaa")
        print(is_match)
        self.assertEqual("aaaaaaaaaaaaaaaaaa@aaaaaaa.aaa is listed on mailhash.domain.invalid", is_match)

class TestRestrictionSet(unittest.TestCase):

    filecontent = """
# comments comments comments comments

# comments comments comments
#

# comments
/\.aaaaaaaaaaaaa\.aaa$/         REJECT

# comments
/aaa\.aaa\.aaaaaaaaaa\.domain\.invalid$/    521  AAAAAAAAAA - comment 1

# comments
/\$/    REJECT

# comments
/^-$/   REJECT

/\.aaaa\.domain\.invalid$/             REJECT  AAAAAAA - comment 2
"""
    filecontent2 = """
/\.servername\.com$/                    REJECT  AAAA comment 3
/\.localhost$/                          REJECT  AAAA comment 4
/\.localdomain$/                        REJECT  AAAA comment 5
/^localhost$/                           REJECT  AAAA comment 6
/\.local$/                              REJECT  AAAA comment 7
/\.lan$/                                REJECT  AAAA comment 8
/\.loc$/                                REJECT  AAAA comment 9
/\.internal$/                           REJECT  AAAA comment 10
/^no-data$/                             REJECT  AAAA comment 11
/\.adsl$/                               REJECT  AAAA comment 12
"""

    filecontent3 = """
/\.localhost$/i                         REJECT  AAAA comment 13
/\.localdomain$/i                       REJECT  AAAA comment 14
/^localhost$/i                          REJECT  AAAA comment 15
/\.local$/                              REJECT  AAAA comment 16
/\.lan$/                                REJECT  AAAA comment 17
/\.loc$/                                REJECT  AAAA comment 18
/\.internal$/                           REJECT  AAAA comment 19
/^aaaaaaa$/                             REJECT  AAAA comment 20
/\.invalid$/                            REJECT  AAAA comment 21
"""

    filecontent4 = """
if /^X-Testheader-Outer/
/KEY1\:KEY2/                      REJECT  AAAA - comment 22
endif
"""

    def test_parse(self):
        """Test regex file line parsing algorithm"""
        lines = TestRestrictionSet.filecontent.split('\n')
        for line in lines:
            print(line)

        restrictions = RegexRestriction.lines2regex(lines=lines)
        self.assertEqual(5, len(restrictions))

    def test_parse2(self):
        """Test regex file line parsing algorithm"""
        lines = TestRestrictionSet.filecontent2.split('\n')
        for line in lines:
            print(line)

        restrictions = RegexRestriction.lines2regex(lines=lines)
        self.assertEqual(10, len(restrictions))

    def test_parse3(self):
        """Test regex file line parsing algorithm with case sensitivity toggle"""
        lines = TestRestrictionSet.filecontent3.split('\n')
        for line in lines:
            print(line)

        restrictions = RegexRestriction.lines2regex(lines=lines)
        self.assertEqual(9, len(restrictions))

    def test_parse4(self):
        """Test regex file line contains an if statement"""
        lines = TestRestrictionSet.filecontent4.split('\n')
        for line in lines:
            print(line)

        restrictions = RegexRestriction.lines2regex(lines=lines)
        self.assertEqual(1, len(restrictions))
        self.assertEqual(2, len(restrictions[0].regex), "The restriction created has 2 regex conditions")

    def test_load(self):
        """Test regex file loading algorithm (during Plugin creation)"""
        setupdict = yaml.safe_load("""
dummy:
   regexfile: /bli/bla/blubb.txt
""")
        with patch.object(RestrictionSet, '_file2lines', return_value=TestRestrictionSet.filecontent.split('\n')):
            rset = RestrictionSet(name="dummy", config=setupdict["dummy"])
        self.assertEqual(5, len(rset.restrictions), "5 rules should have been loaded!")

    def test_hit(self):
        """"""
        setupdict = yaml.safe_load("""
dummy:
   regexfile: /bli/bla/blubb.txt
""")
        with patch.object(RestrictionSet, '_file2lines', return_value=TestRestrictionSet.filecontent.split('\n')):
            rset = RestrictionSet(name="dummy", config=setupdict["dummy"])
        self.assertEqual(5, len(rset.restrictions), "5 rules should have been loaded!")
        res = rset.examine(inputstring="$")
        print(f"Answer is {res}")
        self.assertEqual((lm.REJECT, None), res)

    def test_casesensitive(self):
        """test case sensitive flag"""
        setupdict = yaml.safe_load("""
dummy:
   regexfile: /bli/bla/blubb.txt
""")
        with patch.object(RestrictionSet, '_file2lines', return_value=TestRestrictionSet.filecontent3.split('\n')):
            rset = RestrictionSet(name="dummy", config=setupdict["dummy"])
        res = rset.examine(inputstring="Localhost")
        print(f"Answer is {res}")
        self.assertEqual(None, res)

        res = rset.examine(inputstring="localhost")
        print(f"Answer is {res}")
        self.assertEqual((lm.REJECT, "AAAA comment 15"), res)

class TestRegex(unittest.TestCase):

    lines = """
/\.tester\.domain\.invalid$/             REJECT  AAAAAAAAAAAAAAAAAAAA  - comment
    """.split('\n')

    def test_hit(self):
        restrictions = RegexRestriction.lines2regex(lines=TestRegex.lines)
        self.assertEqual(1, len(restrictions))
        check_match = restrictions[0].match('palim.tester.domain.invalid')
        self.assertTrue(check_match)
        check_nomatch = restrictions[0].match('palim.domain.invalid')
        self.assertFalse(check_nomatch)

    def test_hit_multiregex(self):
        """Test hit with several regex"""
        lines = TestRestrictionSet.filecontent4.split("\n")
        restrictions = RegexRestriction.lines2regex(lines=lines)
        self.assertEqual(1, len(restrictions))
        check_match = restrictions[0].match("X-Testheader-Outer: bli;bla;blubb;KEY1:KEY2;bli;bla")
        self.assertTrue(check_match)

        check_match = restrictions[0].match("X-Testheader-Outer bli;bla;blubb;bli;bla")
        self.assertFalse(check_match)

        check_match = restrictions[0].match("X-TestheaderNomatch-Outer bli;bla;blubb;KEY1:KEY2;bli;bla")
        self.assertFalse(check_match)


    def test_multiregex_noeffecttorest(self):
        """Make sure muliregex doesn't affect other regex rules afterwards"""

        lines = TestRestrictionSet.filecontent4.split("\n") + TestRegex.lines
        for line in lines:
            print(line)
        restrictions = RegexRestriction.lines2regex(lines=lines)
        self.assertEqual(2, len(restrictions))
        check_match = restrictions[1].match('palim.tester.domain.invalid')
        self.assertTrue(check_match)
        check_nomatch = restrictions[1].match('palim.domain.invalid')
        self.assertFalse(check_nomatch)


class TestLint(unittest.TestCase):
    """Lint testers"""
    def test_configfile_not_found(self):
        """Test file not found lint error"""

        confdefaults = """
[test]        
restrictionfile=/tmp/12345_restrictions.yaml
delay_rejects=
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)
        access = AccessRestrictions(conf, section="test")
        self.assertFalse(access.lint(), f"There should be a file does not exist error!")

    def test_noconfigfile_ok(self):
        """no regex file -> lint error"""

        confdefaults = """
[test]        
restrictionfile=
delay_rejects=
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)
        access = AccessRestrictions(conf, section="test")
        self.assertFalse(access.lint(), f"Error if no config file given")

    def test_delay(self):
        """valid delay_rejects milter state"""

        confdefaults = """
[test]        
restrictionfile=/bil/bal/blubb
delay_rejects=rcpt
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)
        access = AccessRestrictions(conf, section="test")
        with patch.object(AccessRestrictions, '_load_yamlfile', return_value={}):
            self.assertTrue(access.lint(), f"No error for valid delay_rejects")

    def test_delay_witherror(self):
        """invalid delay_rejects milter state"""

        confdefaults = """
[test]        
restrictionfile=
delay_rejects=unknown
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)
        access = AccessRestrictions(conf, section="test")
        self.assertFalse(access.lint(), f"There should be an error because of unknown delay state")

    def test_delay_notimpl(self):
        """invalid delay_rejects milter state"""

        confdefaults = """
[test]        
restrictionfile=
delay_rejects=unknown
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)
        access = AccessRestrictions(conf, section="test")
        self.assertFalse(access.lint(), f"There should be an error because of unknown delay state")

    def test_delay_notavailable(self):
        """invalid delay_rejects milter state"""

        confdefaults = """
[test]        
restrictionfile=
delay_rejects=rcpt
state=helo
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)
        access = AccessRestrictions(conf, section="test")
        self.assertFalse(access.lint(), f"State not available in plugin")

    def test_delay_state_notimpl(self):
        """state is not implemented"""

        confdefaults = f"""
[test]        
restrictionfile=
delay_rejects={sm.EOB}
state={sm.EOB}
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)
        access = AccessRestrictions(conf, section="test")
        self.assertFalse(access.lint(), f"Plugin does not implement this state")

    def test_input_notavailable(self):
        """setup state without config should rise a lint error"""

        confdefaults = """
[test]        
restrictionfile=/bli/bla/blubb.yaml
delay_rejects=rcpt
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
restrictions:        
    - name: dummy
      regexfile: /bli/bla/blubb.txt
setup:         
    connect:
""")

        access = AccessRestrictions(conf, section="test")
        with patch.object(AccessRestrictions, '_load_yamlfile', return_value=setupdict):
            with patch.object(RestrictionSet, '_file2lines',
                              return_value=TestRestrictionSet.filecontent.split('\n')):
                self.assertFalse(access.lint(), f"Empty config for 'connect' state in setup should rise error in lint")


    def test_regexerror_lint(self):
        """setup state without config should rise a lint error"""

        confdefaults = """
[test]        
restrictionfile=/bli/bla/blubb.yaml
delay_rejects=
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
restrictions:        
    - name: dummy
      regexfile: /bli/bla/blubb.txt
setup:         
    connect:
        - name: dummy
""")

        access = AccessRestrictions(conf, section="test")
        with patch.object(AccessRestrictions, '_load_yamlfile', return_value=setupdict):
            with patch.object(RestrictionSet, '_file2lines',
                              return_value=["/bli bla blubb"]):
                self.assertFalse(access.lint(),
                                 f"Regex non-match should rise error in lint")

    def test_input_default(self):
        """invalid delay_rejects milter state"""

        confdefaults = """
[test]        
restrictionfile=/bli/bla/blubb.yaml
delay_rejects=rcpt
force_state=
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
restrictions:        
    - name: dummy
      regexfile: /bli/bla/blubb.txt
setup:         
    connect:
        - name: dummy
""")

        access = AccessRestrictions(conf, section="test")
        with patch.object(AccessRestrictions, '_load_yamlfile', return_value=setupdict):
            with patch.object(RestrictionSet, '_file2lines', return_value=TestRestrictionSet.filecontent.split('\n')):
                self.assertTrue(access.lint())

    def test_duplicate_restriction_name(self):
        """Error if defining same name for several restrictions"""

        confdefaults = """
[test]        
restrictionfile=/bli/bla/blubb.yaml
delay_rejects=rcpt
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
restrictions:        
    - name: dummy
      regexfile: /bli/bla/blubb.txt
    - name: dummy
      regexfile: /bli/bla/blubb.txt
setup:         
    connect:
        - name: dummy
""")

        access = AccessRestrictions(conf, section="test")
        with patch.object(AccessRestrictions, '_load_yamlfile', return_value=setupdict):
            with patch.object(RestrictionSet, '_file2lines', return_value=TestRestrictionSet.filecontent.split('\n')):
                self.assertFalse(access.lint())


class TestSubdomainMixin(unittest.TestCase):
    def test_split(self):
        mixin = CreateSubdomainListMixin()
        sdlist = mixin.create_sudomain_list("subdomain.fuglu.org", reverse=True)
        print(sdlist)
        self.assertEqual(['subdomain.fuglu.org', 'fuglu.org'], sdlist)


class TestAccessRestricitons(unittest.TestCase):

    def test_full_relay_delayed(self):
        confdefaults = """
        [test]        
        restrictionfile=/bli/bla/blubb.yaml
        delay_rejects=rcpt
        force_state=
        """
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
        restrictions:        
            - name: dummy
              regexfile: /bli/bla/blubb.txt
        setup:         
            connect:
                - name: dummy
                  input: ptr
            helo:
                - name: dummy
                  input: ptr
        """)

        with patch.object(AccessRestrictions, '_load_yamlfile', return_value=setupdict):
            with patch.object(RestrictionSet, '_file2lines', return_value=TestRestrictionSet.filecontent.split('\n')):
                access = AccessRestrictions(conf, section="test")
                sess = asm.MilterSession(MagicMock(), MagicMock())
                sess.ptr = "$"
                res = access.examine_connect(sess=sess, host="unknown", addr="unknown")

                delaytag = sess.tags.get(AccessRestrictions.DELAY_REJ_TAG)
                print(f"res={res}, MilterSession.tag[{AccessRestrictions.DELAY_REJ_TAG}] = {delaytag}")
                self.assertEqual(res, lm.CONTINUE, "Continue since reject is delayed")
                self.assertEqual((lm.REJECT, None), delaytag, "Reject should be alredy set")

                res = access.examine_helo(sess=sess, helo="helo")
                delaytag = sess.tags.get(AccessRestrictions.DELAY_REJ_TAG)
                print(f"res={res}, MilterSession.tag[{AccessRestrictions.DELAY_REJ_TAG}] = {delaytag}")

                res = access.examine_rcpt(sess=sess, recipient="bli@bla.blubb")
                delaytag = sess.tags.get(AccessRestrictions.DELAY_REJ_TAG)
                print(f"res={res}, MilterSession.tag[{AccessRestrictions.DELAY_REJ_TAG}] = {delaytag}")

    def test_full_relay_delayed_ac(self):
        """Test actioncode"""
        confdefaults = """
        [test]        
        restrictionfile=/bli/bla/blubb.yaml
        delay_rejects=rcpt
        force_state=
        """
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
        restrictions:        
            - name: dummy
              regexfile: /bli/bla/blubb.txt
        setup:         
            connect:
                - name: dummy
                  input: ptr
            helo:
                - name: dummy
                  input: ptr
        """)

        with patch.object(AccessRestrictions, '_load_yamlfile', return_value=setupdict):
            with patch.object(RestrictionSet, '_file2lines', return_value=TestRestrictionSet.filecontent.split('\n')):
                access = AccessRestrictions(conf, section="test")
                sess = asm.MilterSession(MagicMock(), MagicMock())
                sess.ptr = "aaaaa.aaa.aaa.aaaaaaaaaa.domain.invalid"
                res = access.examine_connect(sess=sess, host="unknown", addr="unknown")

                expected_reject = (521, 'AAAAAAAAAA - comment 1')
                delaytag = sess.tags.get(AccessRestrictions.DELAY_REJ_TAG)
                print(f"res={res}, MilterSession.tag[{AccessRestrictions.DELAY_REJ_TAG}] = {delaytag}")
                self.assertEqual(res, lm.CONTINUE, "Continue since reject is delayed")
                self.assertEqual(expected_reject, delaytag, "Reject should be alredy set")

                res = access.examine_helo(sess=sess, helo="helo")
                delaytag = sess.tags.get(AccessRestrictions.DELAY_REJ_TAG)
                print(f"res={res}, MilterSession.tag[{AccessRestrictions.DELAY_REJ_TAG}] = {delaytag}")
                self.assertEqual(res, lm.CONTINUE, "Continue since reject is delayed")

                res = access.examine_rcpt(sess=sess, recipient="bli@bla.blubb")
                delaytag = sess.tags.get(AccessRestrictions.DELAY_REJ_TAG)
                print(f"res={res}, MilterSession.tag[{AccessRestrictions.DELAY_REJ_TAG}] = {delaytag}")
                self.assertEqual(res, expected_reject, "Reject here")

    def test_full_relay_force_state(self):
        """Test forced state, duplicates should be removed"""
        confdefaults = """
        [test]        
        restrictionfile=/bli/bla/blubb.yaml
        force_state=eom
        delay_rejects=
        """
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
        restrictions:        
            - name: dummy
              regexfile: /bli/bla/blubb.txt
            - name: dummy2
              regexfile: /bli/bla/blubb2.txt
        setup:         
            connect:
                - name: dummy
                  input: ptr
            helo:
                - name: dummy
                  input: ptr
                - name: dummy2
                  input: ptr
        """)

        with patch.object(AccessRestrictions, '_load_yamlfile', return_value=setupdict):
            with patch.object(RestrictionSet, '_file2lines', return_value=TestRestrictionSet.filecontent.split('\n')):
                access = AccessRestrictions(conf, section="test")

                self.assertTrue(access.lint())

                sess = asm.MilterSession(MagicMock(), MagicMock())
                sess.addr = "192.168.1.1"
                sess.ptr = "$"
                res = access.examine_connect(sess=sess, host="unknown", addr="unknown")
                self.assertEqual(res, lm.CONTINUE, "Continue since no connect state due to force_state=eom")

                res = access.examine_helo(sess=sess, helo="helo")
                self.assertEqual(res, lm.CONTINUE, "Continue since no connect state due to force_state=eom")

                # overwrite get_ptr routine calculating ptr for Suspect
                AccessRestrictions.get_ptr = lambda x: "$"

                suspect = Suspect("from@fuglu.test", "to@fuglu.test", "/dev/null")
                suspect.clientinfo = force_uString(sess.heloname), force_uString(sess.addr), force_uString(sess.fcrdns)
                _ = access.examine(suspect=suspect)

                print(f"Tag by AccessRestrictions: {suspect.tags.get('blocked', {}).get('AccessRestrictions', False)}")
                print(f"Tag AccessRestrictions block info: {suspect.tags.get('AccessRestrictions.blocked', '<noinfo>')}")
                self.assertTrue(suspect.tags['blocked']["AccessRestrictions"])


    def test_header(self):
        confdefaults = """
        [test]        
        restrictionfile=/bli/bla/blubb.yaml
        delay_rejects=
        force_state=
        """
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
restrictions:        
    - name: headerchecks
      regexfile: /bli/bla/blubb.txt
setup:         
    header:
        - name: headerchecks
""")

        regexfile = """
/^Thread-Topic: Powered By FugluRestrictions/      REJECT
/^Message-ID: \<U\[20/          REJECT          Invalid Msg-ID
"""

        with patch.object(AccessRestrictions, '_load_yamlfile', return_value=setupdict):
            with patch.object(RestrictionSet, '_file2lines', return_value=regexfile.split('\n')):
                access = AccessRestrictions(conf, section="test")
                sess = asm.MilterSession(MagicMock(), MagicMock())

                res = access.examine_header(sess=sess, key=b"Thread-Topic", value=b"Powered By FugluRestrictions")
                self.assertEqual(res, (lm.REJECT, None), "Reject here")

                # remove tags because it would otherwise reject because of the previous reject
                sess.tags = {}

                res = access.examine_header(sess=sess, key=b"Message-ID", value=b"<U[20lkjlkjlkjlkjlkj")
                self.assertEqual(res, (lm.REJECT, "Invalid Msg-ID"), "Reject here")

                # remove tags because it would otherwise reject because of the previous reject
                sess.tags = {}

                res = access.examine_header(sess=sess, key=b"From", value=b"nobody@work")
                self.assertEqual(res, lm.CONTINUE, "Continue, header is not listed")

    def test_suspect_headers(self):
        """Test trigger header and headerchecks"""
        confdefaults = """
[test]        
restrictionfile=/bli/bla/blubb.yaml
delay_rejects=
force_state=
eom_trigger_header=X-TRIGGER
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
restrictions:        
    - name: headerchecks
      regexfile: /bli/bla/blubb.txt
setup:         
    eom:
        - name: headerchecks
          input: headers
""")

        regexfile = """
/^Thread-Topic: Powered By FugluRestrictions/      REJECT   Because of header "${input}"
/^Message-ID: \<U\[20/          REJECT          Invalid Msg-ID
"""

        with patch.object(AccessRestrictions, '_load_yamlfile', return_value=setupdict):
            with patch.object(RestrictionSet, '_file2lines', return_value=regexfile.split('\n')):
                access = AccessRestrictions(conf, section="test")

                filename = os.path.join(TESTDATADIR, 'helloworld.eml')

                suspect = Suspect("from@fuglu.test", "to@fuglu.test", filename)

                _ = access.examine(suspect=suspect)
                self.assertNotIn('AccessRestrictions', suspect.tags['highspam'])

                msg = suspect.get_message_rep()
                msg["Thread-Topic"] = "Powered By FugluRestrictions"
                suspect.set_message_rep(msg)

                _ = access.examine(suspect=suspect)
                self.assertNotIn('AccessRestrictions', suspect.tags['highspam'], "if X-TRIGGER header is missing there's no check")

                msg = suspect.get_message_rep()
                msg["X-TRIGGER"] = "1"
                suspect.set_message_rep(msg)

                _ = access.examine(suspect=suspect)
                self.assertIn('AccessRestrictions', suspect.tags['blocked'])
                self.assertTrue(suspect.tags['blocked']['AccessRestrictions'])
                print(suspect.tags[AccessRestrictions.DELAY_REJ_TAG][1])

    def test_rbl_subdomains(self):
        """Enable checksubdomains option for rbl check"""
        confdefaults = """
        [test]        
        restrictionfile=/bli/bla/blubb.yaml
        delay_rejects=
        force_state=
        """
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
restrictions:        
    - name: rblcheck
      rbl: 
          providertype: "uri-bitmask"
          searchdomain: "rbl.domain.invalid"
          resultconfig:
              - "2:rbl.domain.invalid"
              - "16:rbl.domain.invalid"
          checksubdomains: True
setup:         
    helo:
        - name: rblcheck
          input: heloname
""")

        with patch.object(AccessRestrictions, '_load_yamlfile', return_value=setupdict):
            access = AccessRestrictions(conf, section="test")
            sess = asm.MilterSession(MagicMock(), MagicMock())
            sess.heloname = "bli.bla.blubb.hit.domain.invalid"



            with patch.object(access.restrictions_sets['rblcheck'].restrictions[0].rbllookup.providers[0].resolver, 'lookup', side_effect=TestAccessRestricitons.lookup):
                res = access.examine_helo(sess=sess, helo=sess.heloname)
                print(res)
        self.assertEqual((lm.REJECT, "hit.domain.invalid is listed on rbl.domain.invalid (rbl.domain.invalid)"), res)

    @staticmethod
    def lookup(question, qtype='A'):
        print(f"Lookup: Question is: {question}")
        if question == b"hit.domain.invalid.rbl.domain.invalid":
            dnsres = DNSLookupResult()
            dnsres.content = '127.0.0.2'
            dnsres.qtype = qtype
            dnsres.question = question.decode()
            dnsres.rtype = qtype
            dnsres.ttl = 2627
            return [dnsres]
        else:
            return []

    def test_noptr_match(self):
        confdefaults = """
        [test]        
        restrictionfile=/bli/bla/blubb.yaml
        delay_rejects=
        force_state=
        """
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
restrictions:        
    - name: unknown_reverse_client_hostname
      hash:
         match: unknown
         action: REJECT
         message: ptr is undefined:${input}
setup:         
    connect:
        - name: unknown_reverse_client_hostname
          input: ptr
""")


        with patch.object(AccessRestrictions, '_load_yamlfile', return_value=setupdict):
            access = AccessRestrictions(conf, section="test")
            sess = asm.MilterSession(MagicMock(), MagicMock())
            sess.ptr = "unknown"

            res = access.examine_connect(sess=sess, host="", addr="")
            self.assertEqual(res, (lm.REJECT, "ptr is undefined:unknown"), "Reject here")


    def test_maxheaders_match(self):
        """Test maxheaders count using helperfuncs and also setting of reject message"""
        confdefaults = """
        [test]        
        restrictionfile=/bli/bla/blubb.yaml
        delay_rejects=
        force_state=
        """
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
restrictions:        
    - name: max_hops_exceeded
      hash:
         match: "True"
         action: REJECT
         message: "max hops exceeded ${tag_arraylength} > ${tag_maxlength}"
setup:         
    eoh:
        - name: max_hops_exceeded
          input: headers
          transformations:
              - <func> fuglu.plugins.ratelimit.helperfuncs.filter4left_tuple(filter=received,lowercase=(bool)True,suspect=)
              - <func> fuglu.plugins.ratelimit.helperfuncs.packargs(suspect=)
              - <func> fuglu.plugins.ratelimit.helperfuncs.arraylength_largerthan(maxlength=(int)3,suspect=)
              - <func> fuglu.plugins.ratelimit.helperfuncs.convert_truefalse(suspect=)
""")


        with patch.object(AccessRestrictions, '_load_yamlfile', return_value=setupdict):
            access = AccessRestrictions(conf, section="test")
            lintout = access.lint(state=asm.EOH)
            print(f"lint output: {lintout}")
            sess = asm.MilterSession(MagicMock(), MagicMock())
            sess.original_headers = [
                (b"Received", b"bli bla blubb1"),
                (b"Received", b"bli bla blubb2"),
                (b"Received", b"bli bla blubb3"),
            ]

            res = access.examine_eoh(sess=sess)
            self.assertEqual(res, lm.CONTINUE, "Continue here")

            sess.original_headers.append( (b"Received", b"bli bla blubb1"))
            res = access.examine_eoh(sess=sess)
            self.assertEqual(res, (lm.REJECT, "max hops exceeded 4 > 3"), "Reject here")

    def test_fromheader_domain(self):
        """Extract from-header(s) emails and run rbl checks"""
        confdefaults = """
        [test]        
        restrictionfile=/bli/bla/blubb.yaml
        delay_rejects=
        force_state=
        """
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
restrictions:        
    - name: hdr_from_domain
      rbl: 
          providertype: "uri-bitmask"
          searchdomain: "rbl.domain.invalid"
          resultconfig:
              - "2:rbl.domain.invalid"
              - "16:rbl.domain.invalid"
          checksubdomains: True
setup:         
    eoh:
        - name: hdr_from_domain
          input: headers
          transformations:
              - <func> fuglu.plugins.ratelimit.helperfuncs.decode_from_type_headers(name=from,suspect=)
              - <func> fuglu.plugins.ratelimit.helperfuncs.select_tuple_index(index=(int)1,suspect=)
              - <func> fuglu.plugins.ratelimit.helperfuncs.domain_from_email(suspect=)
""")


        with patch.object(AccessRestrictions, '_load_yamlfile', return_value=setupdict):
            access = AccessRestrictions(conf, section="test")
            lintout = access.lint(state=asm.EOH)
            print(f"lint output: {lintout}")
            self.assertTrue(lintout)
            sess = asm.MilterSession(MagicMock(), MagicMock())

            sess.original_headers = [
                (b"Received", b"bli bla blubb1"),
                (b"Received", b"bli bla blubb3"),
            ]
            with patch.object(access.restrictions_sets['hdr_from_domain'].restrictions[0].rbllookup.providers[0].resolver, 'lookup', side_effect=TestAccessRestricitons.lookup):
                res = access.examine_eoh(sess=sess)
                print(res)
            self.assertEqual(res, lm.CONTINUE, "Continue here")

            sess.original_headers = [
                (b"Received", b"bli bla blubb1"),
                (b"From", b"bli bla <bli.bla@fuglu.org>"),
                (b"From", b"bli bla <bli.bla@hit.domain.invalid>"),
                (b"Received", b"bli bla blubb3"),
            ]

            with patch.object(access.restrictions_sets['hdr_from_domain'].restrictions[0].rbllookup.providers[0].resolver, 'lookup', side_effect=TestAccessRestricitons.lookup):
                res = access.examine_eoh(sess=sess)
                print(res)
            self.assertEqual(res, (lm.REJECT, "hit.domain.invalid is listed on rbl.domain.invalid (rbl.domain.invalid)"), "Reject here")

