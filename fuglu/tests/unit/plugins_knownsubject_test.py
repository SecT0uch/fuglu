# -*- coding: UTF-8 -*-
import unittest
import logging
import sys
import os
from fuglu.plugins.knownsubject import KnownSubjectMixin
from fuglu.shared import Suspect, FuConfigParser
from .unittestsetup import TESTDATADIR


def setup_module():
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)

class TestSpamSubjectMixing(unittest.TestCase):
    def test_specialchars_subject(self,):
        """Test mail subject with "ä" but not encoded"""

        filename = os.path.join(TESTDATADIR, "subject_umlaut.eml")
        config = FuConfigParser()
        config.add_section('test')
        ssubj = KnownSubjectMixin()

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', filename)
        msg = suspect.get_message_rep()
        subject = ssubj._normalise_subject(msg['Subject'], msg['To'])
        self.assertEqual("diezeitlUuftab", subject)

    def test_specialchars_subject2(self,):
        """Test mail subject with special char but no encoding"""

        filename = os.path.join(TESTDATADIR, "subject_nonunichar.eml")
        config = FuConfigParser()
        config.add_section('test')
        ssubj = KnownSubjectMixin()

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', filename)
        msg = suspect.get_message_rep()
        subject = ssubj._normalise_subject(msg['Subject'], msg['To'])
        self.assertEqual(u"klassedasfunktioniertUobsiemitma", subject)
