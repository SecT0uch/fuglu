# -*- coding: UTF-8 -*-
import unittest
import os
import sys
import tempfile
import shutil
import logging
from unittest.mock import patch, MagicMock
from os.path import join, basename
from io import BytesIO
from email.header import decode_header

# PyCharm issue (not loading path correctly, only in debug mode)
UNITTESTDIR = os.path.dirname(os.path.realpath(__file__))
if UNITTESTDIR not in sys.path:
    sys.path.insert(0, UNITTESTDIR)

from .unittestsetup import TESTDATADIR, CONFDIR
from .storedmails import mail_fakeEXEinzip
import fuglu
from fuglu.plugins.attachment import FiletypePlugin, RulesCache, FileHashCheck, FileHashFeeder
from fuglu.shared import actioncode_to_string, Suspect, DELETE, DUNNO, FuConfigParser, FileList


# we import it here to make sure the test system has the library installed
import rarfile


class DatabaseConfigTestCase(unittest.TestCase):

    """Testcases for the Attachment Checker Plugin"""

    def setUp(self):
        testfile = "/tmp/attachconfig.db"
        if os.path.exists(testfile):
            os.remove(testfile)
        # important: 4 slashes for absolute paths!
        testdb = "sqlite:///%s" % testfile

        sql = """create table attachmentrules(
        id integer not null primary key,
        scope varchar(255) not null,
        checktype varchar(20) not null,
        action varchar(255) not null,
        regex varchar(255) not null,
        description varchar(255) not null,
        prio integer not null
        )
        """

        self.session = fuglu.extensions.sql.get_session(testdb)
        self.session.flush()
        self.session.execute(sql)
        self.tempdir = tempfile.mkdtemp('attachtestdb', 'fuglu')
        self.template = '%s/blockedfile.tmpl' % self.tempdir
        shutil.copy(CONFDIR + '/templates/blockedfile.tmpl.dist', self.template)
        shutil.copy(CONFDIR + '/rules/default-filenames.conf.dist',
                    '%s/default-filenames.conf' % self.tempdir)
        shutil.copy(CONFDIR + '/rules/default-filetypes.conf.dist',
                    '%s/default-filetypes.conf' % self.tempdir)
        config = FuConfigParser()
        config.add_section('FiletypePlugin')
        config.set('FiletypePlugin', 'template_blockedfile', self.template)
        config.set('FiletypePlugin', 'rulesdir', self.tempdir)
        config.set('FiletypePlugin', 'dbconnectstring', testdb)
        config.set('FiletypePlugin', 'blockaction', 'DELETE')
        config.set('FiletypePlugin', 'sendbounce', 'True')
        config.set('FiletypePlugin', 'query', 'SELECT action,regex,description FROM attachmentrules WHERE scope=:scope AND checktype=:checktype ORDER BY prio')
        config.add_section('main')
        config.set('main', 'disablebounces', '1')
        config.set('main', 'nobouncefile', '')
        config.set('FiletypePlugin', 'checkarchivenames', 'False')
        config.set('FiletypePlugin', 'checkarchivecontent', 'False')
        config.set('FiletypePlugin', 'archivecontentmaxsize', 500000)
        config.set('FiletypePlugin', 'archiveextractlevel', -1)
        config.set('FiletypePlugin', 'enabledarchivetypes', '')
        self.candidate = FiletypePlugin(config)

    def test_dbrules(self):
        """Test if db rules correctly override defaults"""

        testdata = """
        INSERT INTO attachmentrules(scope,checktype,action,regex,description,prio) VALUES
        ('recipient@unittests.fuglu.org','contenttype','allow','application/x-executable','this user likes exe',1)
        """
        self.session.execute(testdata)
        # copy file rules
        tmpfile = tempfile.NamedTemporaryFile(suffix='virus', prefix='fuglu-unittest', dir='/tmp')
        shutil.copy(TESTDATADIR + '/binaryattachment.eml', tmpfile.name)
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', tmpfile.name)

        result = self.candidate.examine(suspect)
        resstr = actioncode_to_string(result)
        self.assertEqual(resstr, "DUNNO")

        # another recipient should still get the block
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient2@unittests.fuglu.org', tmpfile.name)

        result = self.candidate.examine(suspect)
        if type(result) is tuple:
            result, message = result
        resstr = actioncode_to_string(result)
        self.assertEqual(resstr, "DELETE")
        tmpfile.close()


class AttachmentPluginTestCase(unittest.TestCase):

    """Testcases for the Attachment Checker Plugin"""

    def setUp(self):
        self.tempdir = tempfile.mkdtemp('attachtest', 'fuglu')
        self.template = '%s/blockedfile.tmpl' % self.tempdir
        shutil.copy(
            CONFDIR + '/templates/blockedfile.tmpl.dist', self.template)
        shutil.copy(CONFDIR + '/rules/default-filenames.conf.dist',
                    '%s/default-filenames.conf' % self.tempdir)
        shutil.copy(CONFDIR + '/rules/default-filetypes.conf.dist',
                    '%s/default-filetypes.conf' % self.tempdir)
        config = FuConfigParser()
        config.add_section('FiletypePlugin')
        config.set('FiletypePlugin', 'template_blockedfile', self.template)
        config.set('FiletypePlugin', 'rulesdir', self.tempdir)
        config.set('FiletypePlugin', 'blockaction', 'DELETE')
        config.set('FiletypePlugin', 'sendbounce', 'True')
        config.set('FiletypePlugin', 'checkarchivenames', 'True')
        config.set('FiletypePlugin', 'checkarchivecontent', 'True')
        config.set('FiletypePlugin', 'archivecontentmaxsize', '7000000')
        config.set('FiletypePlugin', 'archiveextractlevel', -1)
        config.set('FiletypePlugin', 'enabledarchivetypes', '')

        config.add_section('main')
        config.set('main', 'disablebounces', '1')
        config.set('main', 'nobouncefile', '')
        self.candidate = FiletypePlugin(config)
        self.rulescache = RulesCache(self.tempdir)
        self.candidate.rulescache = self.rulescache

    def tearDown(self):
        os.remove('%s/default-filenames.conf' % self.tempdir)
        os.remove('%s/default-filetypes.conf' % self.tempdir)
        os.remove(self.template)
        shutil.rmtree(self.tempdir)

    def test_hiddenbinary(self):
        """Test if hidden binaries get detected correctly"""
        # copy file rules
        tmpfile = tempfile.NamedTemporaryFile(suffix='virus', prefix='fuglu-unittest', dir='/tmp')
        shutil.copy(TESTDATADIR + '/binaryattachment.eml', tmpfile.name)
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', tmpfile.name)

        result = self.candidate.examine(suspect)
        if type(result) is tuple:
            result, message = result
        tmpfile.close()
        self.assertEqual(result, DELETE)

    def test_umlaut_in_zip(self):
        """Issue 69: Test if zip with files that contain umlauts are extracted ok"""
        tmpfile = tempfile.NamedTemporaryFile(suffix='badattach', prefix='fuglu-unittest', dir='/tmp')
        shutil.copy(TESTDATADIR + '/umlaut-in-attachment.eml', tmpfile.name)
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', tmpfile.name)

        result = self.candidate.examine(suspect)
        if type(result) is tuple:
            result, message = result
        tmpfile.close()
        self.assertEqual(result, DUNNO)

    def test_special_archive_name(self):
        """Check if gz file with exclamantion marks and points in archive name is extracted ok"""
        import logging
        import sys

        root = logging.getLogger()
        root.setLevel(logging.DEBUG)
        ch = logging.StreamHandler(sys.stdout)
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)
        root.addHandler(ch)

        tmpfile = tempfile.NamedTemporaryFile(
            suffix='badattach', prefix='fuglu-unittest', dir='/tmp')
        shutil.copy(TESTDATADIR + '/attachment_exclamation_marks_points.eml', tmpfile.name)
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', tmpfile.name)

        self.assertTrue('aaa.aa!aaaaaaaaa.aa!2345678910!1234567891.xml' in suspect.att_mgr.get_fileslist(None) )

        result = self.candidate.examine(suspect)
        if type(result) is tuple:
            result, message = result
        tmpfile.close()
        self.assertEqual(result, DUNNO)

    def test_archiveextractsize(self):
        """Test archive extract max filesize"""
        # copy file rules
        for testfile in ['6mbzipattachment.eml', '6mbrarattachment.eml']:
            try:
                tmpfile = tempfile.NamedTemporaryFile(
                    suffix='virus', prefix='fuglu-unittest', dir='/tmp')
                shutil.copy("%s/%s" % (TESTDATADIR, testfile), tmpfile.name)

                user = 'recipient-sizetest@unittests.fuglu.org'
                conffile = self.tempdir + "/%s-archivefiletypes.conf" % user
                # the largefile in the test message is just a bunch of zeroes
                open(conffile, 'w').write("deny application\/octet\-stream no data allowed")
                self.rulescache._loadrules()
                suspect = Suspect('sender@unittests.fuglu.org', user, tmpfile.name)

                # backup old limits from config file
                oldlimit         = self.candidate.config.getint( 'FiletypePlugin', 'archivecontentmaxsize')
                oldlimit_aelevel = self.candidate.config.getint( 'FiletypePlugin', 'archiveextractlevel')

                # now set the limit to 4 mb, the file should be skipped now
                #
                # check log
                # reason of skipping should be the size is to large, file largefile/6mbfile is not extracted
                self.candidate.config.set(
                    'FiletypePlugin', 'archivecontentmaxsize', 4000000)
                result = self.candidate.examine(suspect)
                if type(result) is tuple:
                    result, message = result
                self.assertEqual(result, DUNNO, 'large file should be skipped (not extracted)')

                self.candidate.config.set(
                    'FiletypePlugin', 'archivecontentmaxsize', 7000000)
                result = self.candidate.examine(suspect)
                if type(result) is tuple:
                    result, message = result
                self.assertEqual(
                    result, DELETE, 'extracted large file should be blocked')

                # now set the limit to 5 mb, the file should be skipped now
                # check log
                # reason of skipping should be the size is to large for check, file largefile/6mbfile is already extracted
                self.candidate.config.set(
                    'FiletypePlugin', 'archivecontentmaxsize', 5000000)
                result = self.candidate.examine(suspect)
                if type(result) is tuple:
                    result, message = result
                self.assertEqual(result, DUNNO, 'large file should be skipped')

                # now set the limit to 7 mb, the file should be skipped now
                self.candidate.config.set( 'FiletypePlugin', 'archivecontentmaxsize', 7000000)
                self.candidate.config.set( 'FiletypePlugin', 'archiveextractlevel', 0)

                result = self.candidate.examine(suspect)
                if type(result) is tuple:
                    result, message = result
                self.assertEqual(result, DUNNO, 'large file should be skipped')

                # reset config
                self.candidate.config.set( 'FiletypePlugin', 'archivecontentmaxsize', oldlimit)
                self.candidate.config.set( 'FiletypePlugin', 'archiveextractlevel', oldlimit_aelevel)
            finally:
                tmpfile.close()
                os.remove(conffile)

    def test_archivename(self):
        """Test check archive names"""

        for testfile in ['6mbzipattachment.eml', '6mbrarattachment.eml']:
            try:
                # copy file rules
                tmpfile = tempfile.NamedTemporaryFile(
                    suffix='virus', prefix='fuglu-unittest', dir='/tmp')
                shutil.copy("%s/%s" % (TESTDATADIR, testfile), tmpfile.name)

                user = 'recipient-archivenametest@unittests.fuglu.org'
                conffile = self.tempdir + "/%s-archivenames.conf" % user
                open(conffile, 'w').write(
                    "deny largefile user does not like the largefile within a zip\ndeny 6mbfile user does not like the largefile within a zip")
                self.rulescache._loadrules()
                suspect = Suspect('sender@unittests.fuglu.org', user, tmpfile.name)

                result = self.candidate.examine(suspect)
                if type(result) is tuple:
                    result, message = result
                self.assertEqual(
                    result, DELETE, 'archive containing blocked filename was not blocked')
            finally:
                tmpfile.close()
                os.remove(conffile)

    def test_archivename_nestedarchive(self):
        """Test check archive names in nested archive"""

        #---
        # Note:
        #---
        # mail testedarchive.eml contains the attachment "nestedarchive.tar.gz"
        # which has the following nested structure:
        #---
        # Level : (extracted from archive  ) -> Files
        #---
        # 0     : nestedarchive.tar.gz
        # 1     : (extracting level1.tar.gz) -> level0.txt   level1.tar.gz
        # 2     : (extracting level1.tar.gz) -> level1.txt   level2.tar.gz
        # 3     : (extracting level2.tar.gz) -> level2.txt   level3.tar.gz
        # 4     : (extracting level3.tar.gz) -> level3.txt   level4.tar.gz
        # 5     : (extracting level4.tar.gz) -> level4.txt   level5.tar.gz
        # 6     : (extracting level5.tar.gz) -> level5.txt   level6.tar.gz
        # 7     : (extracting level6.tar.gz) -> level6.txt

        testfile = os.path.join(TESTDATADIR,"nestedarchive.eml")
        try:
            # copy file rules
            user = 'recipient-archivenametest@unittests.fuglu.org'
            conffile = self.tempdir + "/%s-archivenames.conf" % user
            open(conffile, 'w').write(
                "deny level6.txt user does not like the files in nested archives \ndeny 6mbfile user does not like the largefile within a zip")
            self.rulescache._loadrules()

            suspect = Suspect('sender@unittests.fuglu.org', user, testfile)

            oldlimit_aelevel = self.candidate.config.getint( 'FiletypePlugin', 'archiveextractlevel')

            #----
            self.candidate.config.set( 'FiletypePlugin', 'archiveextractlevel', 6)

            result = self.candidate.examine(suspect)
            if type(result) is tuple:
                result, message = result
            self.assertEqual( result, DUNNO, 'archive containing blocked filename should not be extracted')

            #----
            self.candidate.config.set( 'FiletypePlugin', 'archiveextractlevel', 7)

            result = self.candidate.examine(suspect)
            if type(result) is tuple:
                result, message = result
            self.assertEqual( result, DELETE, 'archive containing blocked filename was not blocked')

            self.candidate.config.set( 'FiletypePlugin', 'archiveextractlevel', oldlimit_aelevel)
        finally:
            os.remove(conffile)

    def test_hiddenpart(self):
        """Test for hidden part in message epilogue"""
        testfile='hiddenpart.eml'
        try:
            tmpfile = tempfile.NamedTemporaryFile(
                suffix='hidden', prefix='fuglu-unittest', dir='/tmp')
            shutil.copy("%s/%s" % (TESTDATADIR, testfile), tmpfile.name)

            user = 'recipient-hiddenpart@unittests.fuglu.org'
            conffile = self.tempdir + "/%s-filetypes.conf" % user
            # the largefile in the test message is just a bunch of zeroes
            open(conffile, 'w').write(
                "deny application\/zip no zips allowed")
            self.rulescache._loadrules()
            suspect = Suspect('sender@unittests.fuglu.org', user, tmpfile.name)

            result = self.candidate.examine(suspect)
            if type(result) is tuple:
                result, message = result
            self.assertEqual(
                result, DELETE, 'hidden message part was not detected')

        finally:
            tmpfile.close()
            os.remove(conffile)


    def test_archive_wrong_extension(self):
        """Test if archives don't fool us with forged file extensions"""
        testfile = 'wrongextension.eml'
        try:
            tmpfile = tempfile.NamedTemporaryFile(
                suffix='wrongext', prefix='fuglu-unittest', dir='/tmp')
            shutil.copy("%s/%s" % (TESTDATADIR, testfile), tmpfile.name)

            user = 'recipient-wrongarchextension@unittests.fuglu.org'
            conffile = self.tempdir + "/%s-archivenames.conf" % user
            # the largefile in the test message is just a bunch of zeroes
            open(conffile, 'w').write("deny \.exe$ exe detected in zip with wrong extension")
            self.rulescache._loadrules()
            suspect = Suspect('sender@unittests.fuglu.org', user, tmpfile.name)

            result = self.candidate.examine(suspect)
            if type(result) is tuple:
                result, message = result
            self.assertEqual(
                result, DELETE, 'exe in zip with .gz extension was not detected')

        finally:
            tmpfile.close()
            os.remove(conffile)


class TestOnlyAttachmentInline(unittest.TestCase):

    """
    From bug report. Message text parts (unnamed.txt) detected as dosexec
    and the blocked.
    """

    def setUp(self):
        self.tempdir = tempfile.mkdtemp('attachtest', 'fuglu')
        self.template = '%s/blockedfile.tmpl' % self.tempdir
        shutil.copy(
            CONFDIR + '/templates/blockedfile.tmpl.dist', self.template)
        shutil.copy(CONFDIR + '/rules/default-filenames.conf.dist',
                    '%s/default-filenames.conf' % self.tempdir)
        shutil.copy(CONFDIR + '/rules/default-filetypes.conf.dist',
                    '%s/default-filetypes.conf' % self.tempdir)

        # extend by the content we use for blocking in this test
        with open('%s/default-filetypes.conf' % self.tempdir, "a+") as f:
            f.write("\ndeny	     application\/x-dosexec	    No DOS executables")

        config = FuConfigParser()
        config.add_section('FiletypePlugin')
        config.set('FiletypePlugin', 'template_blockedfile', self.template)
        config.set('FiletypePlugin', 'rulesdir', self.tempdir)
        config.set('FiletypePlugin', 'blockaction', 'DELETE')
        config.set('FiletypePlugin', 'sendbounce', 'True')
        config.set('FiletypePlugin', 'checkarchivenames', 'True')
        config.set('FiletypePlugin', 'checkarchivecontent', 'True')
        config.set('FiletypePlugin', 'archivecontentmaxsize', '7000000')
        config.set('FiletypePlugin', 'archiveextractlevel', -1)
        config.set('FiletypePlugin', 'enabledarchivetypes', '')

        config.add_section('main')
        config.set('main', 'disablebounces', '1')
        config.set('main', 'nobouncefile', '')
        self.candidate = FiletypePlugin(config)
        self.rulescache = RulesCache(self.tempdir)
        self.candidate.rulescache = self.rulescache

    def tearDown(self):
        os.remove('%s/default-filenames.conf' % self.tempdir)
        os.remove('%s/default-filetypes.conf' % self.tempdir)
        os.remove(self.template)
        shutil.rmtree(self.tempdir)

    def test_dosexec(self):
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', TESTDATADIR+'/6mbrarattachment.eml')

        attachmentmanager = suspect.att_mgr
        objectlist = attachmentmanager.get_objectlist()
        filenames = [obj.filename for obj in objectlist]

        self.assertTrue("unnamed.txt" in filenames, "unnamed.txt not in list %s" % ",".join(filenames))
        for obj in objectlist:
            if obj.filename == "unnamed.txt":
                contenttype = obj.contenttype
                # this is a cached property -> patch the cached value for this test
                obj._property_cache['contenttype']='application/x-dosexec'

                # explicitly set to the blocking content
                contenttype = obj.contenttype
                self.assertEqual('application/x-dosexec', contenttype,
                                 "-> check patching of dict for smart_cached_property")
                print("content type: %s" % contenttype)
                print("filename auto generated: %s" % obj.filename_generated)

        result = self.candidate.examine(suspect)
        if type(result) is tuple:
            result, message = result
        self.assertEqual(result, DUNNO, "no attachment should be blocked!")

class AttachmentPluginTestCaseMockBounce(unittest.TestCase):
    """
    Test setup with bouncing enabled. Don't forget to patch SMTP to prevent actual
    sending mail. Patch SMTP is done putting the mocking decorator:

    @patch("smtplib.SMTP") # when working with smtplib directly
    @patch("fuglu.bounce.FugluSMTPClient" # when using fuglu's smtp client from fuglu.bounce module

    in front of the test.
    """
    def setUp(self):
        self.tempdir = tempfile.mkdtemp('attachtest', 'fuglu')
        self.template = '%s/blockedfile.tmpl' % self.tempdir
        shutil.copy(
            CONFDIR + '/templates/blockedfile.tmpl.dist', self.template)
        shutil.copy(CONFDIR + '/rules/default-filenames.conf.dist',
                    '%s/default-filenames.conf' % self.tempdir)
        shutil.copy(CONFDIR + '/rules/default-filetypes.conf.dist',
                    '%s/default-filetypes.conf' % self.tempdir)
        config = FuConfigParser()
        config.add_section('FiletypePlugin')
        config.set('FiletypePlugin', 'template_blockedfile', self.template)
        config.set('FiletypePlugin', 'rulesdir', self.tempdir)
        config.set('FiletypePlugin', 'blockaction', 'DELETE')
        config.set('FiletypePlugin', 'sendbounce', 'True')
        config.set('FiletypePlugin', 'checkarchivenames', 'True')
        config.set('FiletypePlugin', 'checkarchivecontent', 'True')
        config.set('FiletypePlugin', 'archivecontentmaxsize', '7000000')
        config.set('FiletypePlugin', 'archiveextractlevel', -1)
        config.set('FiletypePlugin', 'enabledarchivetypes', '')

        config.add_section('main')
        config.set('main', 'disablebounces', '0')
        config.set('main', 'nobouncefile', '')
        config.set('main', 'outgoingport', '10038')
        config.set('main', 'outgoinghelo', 'test.fuglu.org')
        config.set('main', 'bindaddress', '127.0.0.1')
        config.set('main', 'outgoinghost', '127.0.0.1')
        config.add_section('performance')
        config.set('performance', 'disable_aiosmtp', 'True')
        self.candidate = FiletypePlugin(config)
        self.rulescache = RulesCache(self.tempdir)
        self.candidate.rulescache = self.rulescache

    def tearDown(self):
        os.remove('%s/default-filenames.conf' % self.tempdir)
        os.remove('%s/default-filetypes.conf' % self.tempdir)
        os.remove(self.template)
        shutil.rmtree(self.tempdir)

    @patch("fuglu.bounce.FugluSMTPClient")
    def test_bounce_withenvsender(self, smtpmock):
        """Reference test bounce is called for setup, next test 'test_bounce_noenvsender' should behave differently."""

        testfile = '6mbzipattachment.eml'

        # copy file rules
        tmpfile = tempfile.NamedTemporaryFile(
            suffix='virus', prefix='fuglu-unittest', dir='/tmp')
        shutil.copy("%s/%s" % (TESTDATADIR, testfile), tmpfile.name)

        user = 'recipient-archivenametest@unittests.fuglu.org'
        conffile = self.tempdir + "/%s-archivenames.conf" % user
        open(conffile, 'w').write(
            "deny largefile user does not like the largefile within a zip\ndeny 6mbfile user does not like the largefile within a zip")
        self.rulescache._loadrules()
        suspect = Suspect('sender@unittests.fuglu.org', user, tmpfile.name)

        result = self.candidate.examine(suspect)
        if type(result) is tuple:
            result, message = result
        self.assertEqual(
            result, DELETE, 'archive containing blocked filename was not blocked')
        tmpfile.close()
        os.remove(conffile)

        smtpmock_membercalls = [call[0] for call in smtpmock.mock_calls]
        self.assertTrue(smtpmock.called)
        self.assertTrue("().sendmail" in smtpmock_membercalls)
        self.assertTrue("().quit" in smtpmock_membercalls)

    @patch("fuglu.bounce.FugluSMTPClient")
    def test_bounce_noenvsender(self, smtpmock):
        """Don't try to send a bounce if original env sender is empty"""

        testfile = '6mbzipattachment.eml'

        # copy file rules
        tmpfile = tempfile.NamedTemporaryFile(
            suffix='virus', prefix='fuglu-unittest', dir='/tmp')
        shutil.copy("%s/%s" % (TESTDATADIR, testfile), tmpfile.name)

        user = 'recipient-archivenametest@unittests.fuglu.org'
        conffile = self.tempdir + "/%s-archivenames.conf" % user
        open(conffile, 'w').write(
            "deny largefile user does not like the largefile within a zip\ndeny 6mbfile user does not like the largefile within a zip")
        self.rulescache._loadrules()
        suspect = Suspect('', user, tmpfile.name)

        result = self.candidate.examine(suspect)
        if type(result) is tuple:
            result, message = result
        self.assertEqual(
            result, DELETE, 'archive containing blocked filename was not blocked')
        tmpfile.close()
        os.remove(conffile)

        self.assertFalse(smtpmock.called, "No SMTP client should have been created because there's no mail to send.")


class AttachmentArchivesPluginTestCase(unittest.TestCase):

    """Testcases for the Attachment Checker Plugin"""

    def setUp(self):
        self.tempdir = tempfile.mkdtemp('attachtest', 'fuglu')
        self.template = '%s/blockedfile.tmpl' % self.tempdir
        shutil.copy(
            CONFDIR + '/templates/blockedfile.tmpl.dist', self.template)
        shutil.copy(CONFDIR + '/rules/default-filenames.conf.dist',
                    '%s/default-filenames.conf' % self.tempdir)
        shutil.copy(CONFDIR + '/rules/default-filetypes.conf.dist',
                    '%s/default-filetypes.conf' % self.tempdir)

        with open(f"{self.tempdir}/default-archivefiletypes.conf", 'w') as f:
            f.write(
                """# testing...
deny         executable     No programs allowed (exe)
deny         ELF            No programs allowed (ELF)
deny         application\/x-dosexec     No DOS executables
deny         application\/x-msi No MSI executables allowed
deny         application\/x-msdos-program       No DOS executables               
deny         application\/x-sharedlib       No shared libs/dyn linked programs
"""
            )
        config = FuConfigParser()
        config.add_section('FiletypePlugin')
        config.set('FiletypePlugin', 'template_blockedfile', self.template)
        config.set('FiletypePlugin', 'rulesdir', self.tempdir)
        config.set('FiletypePlugin', 'blockaction', 'DELETE')
        config.set('FiletypePlugin', 'sendbounce', 'False')
        config.set('FiletypePlugin', 'checkarchivenames', 'True')
        config.set('FiletypePlugin', 'checkarchivecontent', 'True')
        config.set('FiletypePlugin', 'archivecontentmaxsize', '7000000')
        config.set('FiletypePlugin', 'archiveextractlevel', -1)
        config.set('FiletypePlugin', 'enabledarchivetypes', 'zip, z, rar, tar, 7z, gz')

        config.add_section('main')
        config.set('main', 'disablebounces', '1')
        config.set('main', 'nobouncefile', '')
        config.add_section('performance')
        config.set('performance', 'disable_aiosmtp', 'True')
        self.candidate = FiletypePlugin(config)
        self.rulescache = RulesCache(self.tempdir)
        self.candidate.rulescache = self.rulescache

    def tearDown(self):
        os.remove(f'{self.tempdir}/default-filenames.conf')
        os.remove(f'{self.tempdir}/default-filetypes.conf')
        os.remove(f"{self.tempdir}/default-archivefiletypes.conf")
        os.remove(self.template)
        shutil.rmtree(self.tempdir)

    def test_block_exe_in_gz(self):
        # Test blocking of a dynamically linked executable in archive due to sharedlib rule

        from html.parser import HTMLParser
        from email.mime.application import MIMEApplication
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText
        from email.utils import formatdate
        from email.header import Header

        msg = MIMEMultipart()
        msg['From'] = "sender@fuglu.org"
        msg['To'] = "receiver@fuglu.org"
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = "whatever..."

        msg.attach(MIMEText("This is some text..."))

        files = [join(TESTDATADIR, "helloworld.gz")]

        for f in files or []:
            with open(f, "rb") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=basename(f)
                )
            hdr = Header('attachment', header_name="Content-Disposition", continuation_ws=' ')
            part["Content-Disposition"] = hdr
            msg.attach(part)

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', "/dev/null")
        suspect.set_message_rep(msg)

        result = self.candidate.examine(suspect)
        message = ""
        if type(result) is tuple:
            result, message = result
            print(message)
        resstr = actioncode_to_string(result)
        self.assertEqual(resstr, "DELETE")
        self.assertEqual(message, "helloworld application/x-sharedlib: No shared libs/dyn linked programs")

    def test_block_staticexe_in_gz(self):
        # Test blocking of a statically linked executable in archive due to executable rule

        from html.parser import HTMLParser
        from email.mime.application import MIMEApplication
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText
        from email.utils import formatdate
        from email.header import Header

        msg = MIMEMultipart()
        msg['From'] = "sender@fuglu.org"
        msg['To'] = "receiver@fuglu.org"
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = "whatever..."

        msg.attach(MIMEText("This is some text..."))

        files = [join(TESTDATADIR, "helloworldstatic.gz")]

        for f in files or []:
            with open(f, "rb") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=basename(f)
                )
            hdr = Header('attachment', header_name="Content-Disposition", continuation_ws=' ')
            part["Content-Disposition"] = hdr
            msg.attach(part)

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', "/dev/null")
        suspect.set_message_rep(msg)

        result = self.candidate.examine(suspect)
        message = ""
        if type(result) is tuple:
            result, message = result
            print(message)
        resstr = actioncode_to_string(result)
        self.assertEqual(resstr, "DELETE")
        self.assertEqual(message, "helloworldstatic application/x-executable: No programs allowed (exe)")


class AttachmentEncryptedArchivesPluginTestCase(unittest.TestCase):

    """Testcases for the Attachment Checker Plugin"""

    tempdir = None
    template = None
    candidate = None
    rulescache = None

    @classmethod
    def setUpClass(cls) -> None:
        cls.tempdir = tempfile.mkdtemp('attachtest', 'fuglu')
        cls.template = join(cls.tempdir, 'blockedfile.tmpl')

        src = join(*[CONFDIR, "templates", "blockedfile.tmpl.dist"])
        shutil.copy(src, cls.template)

        src = join(*[CONFDIR, "rules", "default-filenames.conf.dist"])
        tar = join(cls.tempdir, 'default-filenames.conf')
        shutil.copy(src, tar)

        src = join(*[CONFDIR, "rules", "default-filetypes.conf.dist"])
        tar = join(cls.tempdir, "default-filetypes.conf")
        shutil.copy(src, tar)

        src = join(*[CONFDIR, "rules", "default-archivefiletypes.conf.dist"])
        tar = join(cls.tempdir, "default-archivefiletypes.conf")
        shutil.copy(src, tar)

        src = join(*[CONFDIR, "rules", "default-archivenames.conf.dist"])
        tar = join(cls.tempdir, "default-archivenames.conf")
        shutil.copy(src, tar)

        tar = join(cls.tempdir, "default-archivenames-crypto.conf")
        with open(tar, 'w') as f:
            f.write(
                """# testing...
                
deny .doc$ encrypted archive contains evil doc
"""
            )
        config = FuConfigParser()
        config.add_section('FiletypePlugin')
        config.set('FiletypePlugin', 'template_blockedfile', cls.template)
        config.set('FiletypePlugin', 'rulesdir', cls.tempdir)
        config.set('FiletypePlugin', 'blockaction', 'DELETE')
        config.set('FiletypePlugin', 'sendbounce', 'False')
        config.set('FiletypePlugin', 'checkarchivenames', 'True')
        config.set('FiletypePlugin', 'checkarchivecontent', 'True')
        config.set('FiletypePlugin', 'archivecontentmaxsize', '7000000')
        config.set('FiletypePlugin', 'archiveextractlevel', -1)
        config.set('FiletypePlugin', 'enabledarchivetypes', 'zip, z, rar, tar, 7z, gz')

        config.add_section('main')
        config.set('main', 'disablebounces', '1')
        config.set('main', 'nobouncefile', '')
        config.add_section('performance')
        config.set('performance', 'disable_aiosmtp', 'True')
        cls.candidate = FiletypePlugin(config)
        cls.rulescache = RulesCache(cls.tempdir, nocache=True)
        cls.candidate.rulescache = cls.rulescache

    @classmethod
    def tearDownClass(cls) -> None:
        os.remove(f'{cls.tempdir}/default-filenames.conf')
        os.remove(f'{cls.tempdir}/default-filetypes.conf')
        os.remove(f"{cls.tempdir}/default-archivefiletypes.conf")
        os.remove(f"{cls.tempdir}/default-archivenames.conf")
        os.remove(f"{cls.tempdir}/default-archivenames-crypto.conf")
        os.remove(cls.template)
        shutil.rmtree(cls.tempdir)

    def test_block_doc_in_pwarchive(self):
        """ Test blocking of a doc file in a password protected zip """

        from html.parser import HTMLParser
        from email.mime.application import MIMEApplication
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText
        from email.utils import formatdate
        from email.header import Header

        msg = MIMEMultipart()
        msg['From'] = "sender@fuglu.org"
        msg['To'] = "receiver@fuglu.org"
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = "whatever..."

        msg.attach(MIMEText("This is some text..."))

        files = [join(TESTDATADIR, "dummy.doc.zip")]

        for f in files or []:
            with open(f, "rb") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=basename(f)
                )
            hdr = Header('attachment', header_name="Content-Disposition", continuation_ws=' ')
            part["Content-Disposition"] = hdr
            msg.attach(part)

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', "/dev/null")
        suspect.set_message_rep(msg)

        result = self.candidate.examine(suspect)
        message = ""
        if type(result) is tuple:
            result, message = result
            print(message)
        resstr = actioncode_to_string(result)
        self.assertEqual(resstr, "DELETE")
        #self.assertEqual(resstr, "DUNNO")
        self.assertEqual(message, "dummy.doc: encrypted archive contains evil doc")

    def test_noblock_txt_in_pwarchive(self):
        """Test if new blocking rules in pw-protected zip does not block harmless txt file"""

        from html.parser import HTMLParser
        from os.path import join, basename
        from email.mime.application import MIMEApplication
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText
        from email.utils import formatdate
        from email.header import Header

        msg = MIMEMultipart()
        msg['From'] = "sender@fuglu.org"
        msg['To'] = "receiver@fuglu.org"
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = "whatever..."

        msg.attach(MIMEText("This is some text..."))

        files = [join(TESTDATADIR, "test.txt.pwd.zip")]

        for f in files or []:
            with open(f, "rb") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=basename(f)
                )
            hdr = Header('attachment', header_name="Content-Disposition", continuation_ws=' ')
            part["Content-Disposition"] = hdr
            msg.attach(part)

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', "/dev/null")
        suspect.set_message_rep(msg)

        result = self.candidate.examine(suspect)
        message = ""
        if type(result) is tuple:
            result, message = result
            print(message)
        resstr = actioncode_to_string(result)
        self.assertEqual(resstr, "DUNNO")

    def test_noblock_doc_in_archive(self):
        """ Test non-blocking of a doc file in a non-password protected zip """

        from html.parser import HTMLParser
        from email.mime.application import MIMEApplication
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText
        from email.utils import formatdate
        from email.header import Header

        msg = MIMEMultipart()
        msg['From'] = "sender@fuglu.org"
        msg['To'] = "receiver@fuglu.org"
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = "whatever..."

        msg.attach(MIMEText("This is some text..."))

        files = [join(TESTDATADIR, "plain.doc.zip")]

        for f in files or []:
            with open(f, "rb") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=basename(f)
                )
            hdr = Header('attachment', header_name="Content-Disposition", continuation_ws=' ')
            part["Content-Disposition"] = hdr
            msg.attach(part)

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', "/dev/null")
        suspect.set_message_rep(msg)

        result = self.candidate.examine(suspect)
        message = ""
        if type(result) is tuple:
            result, message = result
            print(message)
        resstr = actioncode_to_string(result)
        self.assertEqual(resstr, "DUNNO")



class TestFileHashCheck(unittest.TestCase):
    """
    Tests for TestFileHashCheck with a fixed positive answer for _check_hash
    """

    def setUp(self):
        """
        Set up TestFileHashCheck, mock the "_check_hash" routine to return "test_test_test"
        """

        myclass = self.__class__.__name__
        self.logger = logging.getLogger(myclass)

        self.logger.debug("Config parser")
        conf = FuConfigParser()

        handle, self.filename_extensions = tempfile.mkstemp(
            prefix="filehashtest", dir='/tmp')

        with os.fdopen(handle, 'w+b') as file_extensions:
            file_extensions.write(b"zip\n")

        confdefaults = ("""
[test]
redis_conn = redis://127.0.0.1:6379/1
timeout = 2
hashtype = MD5
hashskiplistfile =
extensionsfile = %s
filenameskipfile =
allowmissingextension = False
minfilesize = 100
""" % self.filename_extensions)
        conf.read_string(confdefaults)

        self.filehashclient = FileHashCheck(conf, section='test')

        self.filehashclient._check_hash = MagicMock()
        self.filehashclient._check_hash.return_value = "test_test_test"

    def test_examine(self):
        """Test examine with a fixed list answer"""

        self.logger.debug("Read file content")
        filecontent = BytesIO(mail_fakeEXEinzip).read()
        expecteddict = {'fakeEXE.zip': 'test_test_test f6578bc9979270a79358d9272aa19968'}

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', "/dev/null")
        suspect.set_source(filecontent)

        self.filehashclient.examine(suspect)
        self.assertTrue(suspect.tags.get('blocked', {}).get('FileHashCheck'), 'not set as blocked')
        answerdict = suspect.get_tag('FileHashCheck.blocked')
        for key in expecteddict.keys():
            print("%s : %s/%s" % (key, answerdict.get(key), expecteddict[key]))
            self.assertEqual(expecteddict[key], answerdict.get(key))

    def test_examine_unicode_attachment(self):
        """Test examine for a unicode named attachement"""

        self.logger.debug("Read file content")
        attname = "=?UTF-8?Q?cha=cc=88schu=cc=88echli.zip?="
        attnamebytes, encoding = decode_header(attname)[0]
        attnamestr = attnamebytes.decode(encoding)
        expecteddict = {attnamestr: 'test_test_test db837166f8a3459418f254712601cf84'}
        filename = os.path.join(TESTDATADIR, "umlaut-in-attachment.eml")

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', filename)
        self.filehashclient.examine(suspect)
        self.assertTrue(suspect.tags.get('blocked', {}).get('FileHashCheck'), 'not set as blocked')
        
        answerdict = suspect.get_tag('FileHashCheck.blocked')
        for key in expecteddict.keys():
            print("%s : %s/%s" % (key, answerdict.get(key), expecteddict[key]))
            self.assertEqual(expecteddict[key], answerdict.get(key))


class TestFileHashFeeder(unittest.TestCase):
    """Test for attachments, mock redis insert function for testing"""
    def setUp(self):
        """
        Setup HaschmasterFeeder, mock init_redis function
        """

        myclass = self.__class__.__name__
        self.logger = logging.getLogger(myclass)

        self.logger.debug("Config parser")
        conf = FuConfigParser()

        handle, self.filename_extensions = tempfile.mkstemp(
            prefix="filehashfeeder", dir='/tmp')

        with os.fdopen(handle, 'w+b') as file_extensions:
            file_extensions.write(b"zip\n")

        confdefaults = ("""
[test]
redis_conn = redis://127.0.0.1:6379/1
timeout = 2
hashtype = MD5
hashskiplistfile =
extensionsfile = %s
filenameskipfile =
allowmissingextension = False
minfilesize = 100
expirationdays = 3
""" % self.filename_extensions)
        conf.read_string(confdefaults)

        self.filehashfeeder = FileHashFeeder(conf, section='test')

        hashtype = self.filehashfeeder.config.get(self.filehashfeeder.section, 'hashtype')
        hashtype = hashtype.lower()

        import hashlib
        if not hasattr(hashlib, hashtype):
            self.logger.error('invalid hash type %s' % hashtype)

        if self.filehashfeeder.minfilesizebyext is None:
            self.filehashfeeder.minfilesizebyext = dict(zip=40)
        
        if self.filehashfeeder.hashskiplist is None:
            self.filehashfeeder.hashskiplist = FileList(
                filename=self.filehashfeeder.config.get(self.filehashfeeder.section, 'hashskiplistfile'),
                lowercase=True, additional_filters=[FileList.inline_comments_filter])

        if self.filehashfeeder.extensions is None:
            self.filehashfeeder.extensions = FileList(
                filename=self.filehashfeeder.config.get(self.filehashfeeder.section, 'extensionsfile'),
                lowercase=True, additional_filters=[FileList.inline_comments_filter])

        if self.filehashfeeder.filenameskip is None:
            self.filehashfeeder.filenameskip = FileList(
                filename=self.filehashfeeder.config.get(self.filehashfeeder.section, 'filenameskipfile'),
                lowercase=True, additional_filters=[FileList.inline_comments_filter])

        self.filehashfeeder._insert_redis = MagicMock()

    def test_examine(self):
        """Test examine if feeder tries to insert hash to redis database by mocking insert function"""

        self.logger.debug("Read file content")
        filecontent = BytesIO(mail_fakeEXEinzip).read()

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', "/dev/null")
        suspect.set_source(filecontent)

        dummy = ""
        self.filehashfeeder.process(suspect, dummy)
        # check mock
        import json
        callargs = self.filehashfeeder._insert_redis.call_args[0]
        key = callargs[0]
        data = json.loads(callargs[1])
        
        self.assertEqual(1, self.filehashfeeder._insert_redis.call_count)
        self.assertEqual('f6578bc9979270a79358d9272aa19968', key)
        self.assertEqual("fakeEXE.zip", data.get('filename'))
        self.assertEqual(key, data.get('filehash'))
        self.assertEqual(208, data.get('filesize'))
