# -*- coding: UTF-8 -*-
import unittest
import os
from fuglu.shared import Suspect, FuConfigParser
from fuglu.plugins.outpolicy import FuzorRateLimit
import tempfile
from .unittestsetup import TESTDATADIR


class TestFuzorRateLimit(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.tmpfile_sender = tempfile.NamedTemporaryFile()
        cls.tmpfile_alert = tempfile.NamedTemporaryFile()
        confdefaults = f"""
        [test]
        sender_exception_file = {cls.tmpfile_sender.name}
        alert_exception_file = {cls.tmpfile_alert.name}
        """
        conf = FuConfigParser()
        conf.read_string(confdefaults)
        cls.config = conf

    def test_nosubject(self,):
        """
        Ignore empty subject keyword check for empty subjects
        """

        tmpfile_sender = tempfile.NamedTemporaryFile()
        tmpfile_alert = tempfile.NamedTemporaryFile()
        # use special config because threshold has to be 0 such that
        # because otherwise the subject is not used for testing keywords
        confdefaults = f"""
        [test]
        sender_exception_file = {tmpfile_sender.name}
        alert_exception_file = {tmpfile_alert.name}
        threshold = 0
        """
        conf = FuConfigParser()
        conf.read_string(confdefaults)

        filename = os.path.join(TESTDATADIR, "subject_umlaut.eml")

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', filename)
        msg = suspect.get_message_rep()
        del msg["Subject"]
        self.assertNotIn("Subject", msg.keys())

        suspect.set_source(msg.as_bytes(), att_mgr_reset=True)

        frl = FuzorRateLimit(conf, "test")
        frl.examine(suspect)

    def test_lint(self,):
        confdefaults = "[test]"
        conf = FuConfigParser()
        conf.read_string(confdefaults)

        frl = FuzorRateLimit(self.config, "test")
        out = frl.lint()
        print(out)
