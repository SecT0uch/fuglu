# -*- coding: UTF-8 -*-
import unittest
import tempfile
import os
import sys

# PyCharm issue (not loading path correctly, only in debug mode)
UNITTESTDIR = os.path.dirname(os.path.realpath(__file__))
if UNITTESTDIR not in sys.path:
    sys.path.insert(0, UNITTESTDIR)

from .unittestsetup import TESTDATADIR
from fuglu.shared import Suspect, DUNNO, REJECT, FuConfigParser
from fuglu.plugins.domainauth import SPFPlugin, SpearPhishPlugin, SenderRewriteScheme, SRS_AVAILABLE, DMARCPlugin, DMARC_AVAILABLE, \
    DKIMVerifyPlugin, DKIM_VALID, DKIM_VALID_SENDER, DKIM_VALID_AUTHOR, DKIM_INVALID, SPFOut
from dkim import DKIM
from email.mime.multipart import MIMEMultipart


class SPFTestCase(unittest.TestCase):

    """SPF Check Tests"""

    def _make_dummy_suspect(self, senderdomain, clientip, helo='foo.example.com'):
        s = Suspect('sender@%s' % senderdomain, 'recipient@unittests.fuglu.org', '/dev/null')
        s.clientinfo = (helo, clientip, 'ptr.example.com')
        return s

    def setUp(self):
        config = FuConfigParser()
        config.add_section('SPFPlugin')
        config.set('SPFPlugin', 'max_lookups', '10')
        config.set('SPFPlugin', 'skiplist', '')
        config.set('SPFPlugin', 'temperror_retries', '10')
        config.set('SPFPlugin', 'temperror_sleep', '10')
        
        self.candidate = SPFPlugin(config)

    def tearDown(self):
        pass

    def testSPF(self):
        # TODO: for now we use gmail.com as spf test domain with real dns
        # lookups - replace with mock

        # google fail test

        suspect = self._make_dummy_suspect('gmail.com', '1.2.3.4')
        self.candidate.examine(suspect)
        self.assertEquals(suspect.get_tag('SPF.status'), 'softfail')

        suspect = self._make_dummy_suspect('gmail.com', '216.239.32.22')
        self.candidate.examine(suspect)
        self.assertEquals(suspect.get_tag('SPF.status'), 'pass')

        # no spf record
        suspect = self._make_dummy_suspect('unittests.fuglu.org', '1.2.3.4')
        self.candidate.examine(suspect)
        self.assertEqual(suspect.get_tag('SPF.status'), 'none')


class SpearPhishTestCase(unittest.TestCase):
    """Spearphish Plugin Tests"""

    def _make_dummy_suspect(self, envelope_sender_domain='a.unittests.fuglu.org', header_from_domain='a.unittests.fuglu.org', recipient_domain='b.unittests.fuglu.org', file='/dev/null'):
        s = Suspect('sender@%s' % envelope_sender_domain, 'recipient@%s'%recipient_domain, file)

        template="""From: sender@%s
Subject: Hello spear phished world!
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary="----=_MIME_BOUNDARY_000_12140"

------=_MIME_BOUNDARY_000_12140
Content-Type: text/plain

blablabla

some <tagged>text</tagged>
------=_MIME_BOUNDARY_000_12140--
        """%header_from_domain

        s.set_source(template)
        return s

    def _make_config(self, checkdomains=None, virusname='UNITTEST-SPEARPHISH', virusaction='REJECT', virusenginename='UNIITEST Spearphishing protection', rejectmessage='threat detected: ${virusname}', check_display_part='True', check_bounces='True'):
        config = FuConfigParser()
        config.add_section('SpearPhishPlugin')

        if checkdomains:
            tempfilename = tempfile.mktemp(
                suffix='spearphish', prefix='fuglu-unittest', dir='/tmp')
            fp = open(tempfilename, 'w')
            fp.write('\n'.join(checkdomains))
            self.tempfiles.append(tempfilename)
            config.set('SpearPhishPlugin', 'domainsfile', tempfilename)
        else:
            config.set('SpearPhishPlugin', 'domainsfile', '')
        config.set('SpearPhishPlugin', 'virusname', virusname)
        config.set('SpearPhishPlugin', 'virusaction', virusaction)
        config.set('SpearPhishPlugin', 'virusenginename', virusenginename)
        config.set('SpearPhishPlugin', 'rejectmessage', rejectmessage)
        config.set('SpearPhishPlugin', 'dbconnection', '')
        config.set('SpearPhishPlugin', 'domain_sql_query', '')
        config.set('SpearPhishPlugin', 'check_display_part', check_display_part)
        config.set('SpearPhishPlugin', 'checkbounces', check_bounces)
        return config


    def setUp(self):
        self.tempfiles = []


    def tearDown(self):
        for tempfile in self.tempfiles:
            os.remove(tempfile)

    def test_check_specific_domains(self):
        """Test if only domains from the config file get checked"""
        shouldcheck = ['evil1.unittests.fuglu.org', 'evil2.unittests.fuglu.org']
        shouldnotcheck = ['evil11.unittests.fuglu.org', 'evil22.unittests.fuglu.org']

        config = self._make_config(checkdomains=shouldcheck, virusaction='REJECT', rejectmessage='spearphish')
        candidate = SpearPhishPlugin(None)
        candidate.config = config

        for domain in shouldcheck:
            suspect = self._make_dummy_suspect(envelope_sender_domain='example.com', recipient_domain=domain, header_from_domain=domain)
            self.assertEqual(candidate.examine(suspect), (REJECT, 'spearphish'), ' spearphish should have been detected')

        for domain in shouldnotcheck:
            suspect = self._make_dummy_suspect(envelope_sender_domain='example.com', recipient_domain=domain,
                                               header_from_domain=domain)
            self.assertEqual(candidate.examine(suspect), DUNNO, 'spearphish should have been ignored - not in config file' )

    def test_multiline(self):
        """Check a multiline from header"""
        shouldcheck = ['evil1.unittests.fuglu.org', 'evil2.unittests.fuglu.org']
        config = self._make_config(checkdomains=shouldcheck, virusaction='REJECT', rejectmessage='spearphish')
        candidate = SpearPhishPlugin(None)
        candidate.config = config

        domain = 'evil1.unittests.fuglu.org'
        envelope_sender_domain = 'example.com'
        recipient_domain = domain
        file = os.path.join(TESTDATADIR, "from_subject_2lines.eml")
        suspect = Suspect('sender@%s' % envelope_sender_domain, 'recipient@%s' % recipient_domain, file)

        response = candidate.examine(suspect)
        self.assertEqual(response, (REJECT, 'spearphish'), ' spearphish should have been detected')

    def test_check_all_domains(self):
        """Test if all domains are checked if an empty file is configured"""
        shouldcheck = ['evil1.unittests.fuglu.org', 'evil2.unittests.fuglu.org']

        config = self._make_config(checkdomains=[], virusaction='REJECT', rejectmessage='spearphish')
        candidate = SpearPhishPlugin(None)
        candidate.config = config

        for domain in shouldcheck:
            suspect = self._make_dummy_suspect(envelope_sender_domain='example.com', recipient_domain=domain,
                                               header_from_domain=domain)
            self.assertEqual(candidate.examine(suspect), (REJECT, 'spearphish'),
                             ' spearphish should have been detected')

    def test_emptyfrom(self):
        """Check with empty mail but address in display part"""
        shouldcheck = ['evil1.unittests.fuglu.org', 'evil2.unittests.fuglu.org']
        config = self._make_config(checkdomains=shouldcheck, virusaction='REJECT', rejectmessage='spearphish', check_display_part='True')
        candidate = SpearPhishPlugin(None)
        candidate.config = config

        domain = 'evil1.unittests.fuglu.org'
        envelope_sender_domain = 'example.com'
        recipient_domain = domain
        file = os.path.join(TESTDATADIR, "empty_from_to.eml")
        suspect = Suspect('sender@%s' % envelope_sender_domain, 'recipient@%s' % recipient_domain, file)

        response = candidate.examine(suspect)
        self.assertEqual(response, (REJECT, 'spearphish'), ' spearphish should have been detected')

    def test_commercial_at(self):
        """check hit on 'FULLWIDTH COMMERCIAL AT' in display part

        Display Part of From-Header: 'John Doe <john.doe＠fuglu.org>'
        """

        config = self._make_config(checkdomains=[],
                                   virusaction='REJECT',
                                   rejectmessage='spearphish',
                                   check_display_part='True')
        candidate = SpearPhishPlugin(None)
        candidate.config = config

        file = os.path.join(TESTDATADIR, "spearphish_fullwidth_commercial_at.eml")
        suspect = Suspect('spearphish@example.com', 'recipient@fuglu.org', file)

        response = candidate.examine(suspect)
        self.assertEqual(response, (REJECT, 'spearphish'), ' spearphish should have been detected')


    def test_specification(self):
        """Check if the plugin works as intended:
        Only hit if header_from_domain = recipient domain but different envelope sender domain
        """
        config = self._make_config(checkdomains=[], virusaction='REJECT', rejectmessage='spearphish')
        candidate = SpearPhishPlugin(None)
        candidate.config = config

        # the spearphish case, header from = recipient, but different env sender
        self.assertEqual(candidate.examine(
            self._make_dummy_suspect(
                envelope_sender_domain='a.example.com',
                recipient_domain='b.example.com',
                header_from_domain='b.example.com')),
            (REJECT, 'spearphish'),
            'spearphish should have been detected')

        # don't hit if env sender matches as well
        self.assertEqual(candidate.examine(
            self._make_dummy_suspect(
                envelope_sender_domain='c.example.com',
                recipient_domain='c.example.com',
                header_from_domain='c.example.com')),
            DUNNO,
            'env sender domain = recipient domain should NOT be flagged as spearphish (1)')

        # don't hit if all different
        self.assertEqual(candidate.examine(
            self._make_dummy_suspect(
                envelope_sender_domain='d.example.com',
                recipient_domain='e.example.com',
                header_from_domain='f.example.com')),
            (DUNNO, None),
            'env sender domain = recipient domain should NOT be flagged as spearphish (2)')


class SRSTests(unittest.TestCase):
    """SenderRewriteScheme Tests"""

    def base_test_rewrite(self):
        """Test sender rewrite"""

        self.assertTrue(SRS_AVAILABLE)

        config = FuConfigParser()
        config.add_section('SenderRewriteScheme')

        # 'default': "mysql://root@localhost/spfcheck?charset=utf8",
        # 'description': 'SQLAlchemy Connection string. Leave empty to rewrite all senders',
        config.set('SenderRewriteScheme', 'dbconnection', '')

        # 'default': "SELECT use_srs from domain where domain_name=:domain",
        # 'description': 'get from sql database :domain will be replaced with the actual domain name. must return field use_srs',
        config.set('SenderRewriteScheme', 'domain_sql_query', "SELECT use_srs from domain where domain_name=:domain")

        # 'default': 'example.com',
        # 'description': 'the new envelope sender domain',
        config.set('SenderRewriteScheme', 'forward_domain', "srs.fuglu.org")

        # 'default': '',
        # 'description': 'cryptographic secret. set the same random value on all your machines',
        config.set('SenderRewriteScheme', 'secret', "")

        # 'default': '8',
        # 'description': 'maximum lifetime of bounces',
        config.set('SenderRewriteScheme', 'maxage', "8")

        # 'default': '8',
        # 'description': 'size of auth code',
        config.set('SenderRewriteScheme', 'hashlength', "8")

        # 'default': '=',
        # 'description': 'SRS token separator',
        config.set('SenderRewriteScheme', 'separator', "=")

        # 'default': 'True',
        # 'description': 'set True to rewrite address in To: header in bounce messages (reverse/decrypt mode)',
        config.set('SenderRewriteScheme', 'rewrite_header_to', True)

        srs = SenderRewriteScheme(config, section="SenderRewriteScheme")

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', "/dev/null")
        srs.examine(suspect)
        self.assertTrue("SRS" in suspect.from_localpart, "%s" % suspect.from_localpart)
        self.assertEqual("srs.fuglu.org", suspect.from_domain, "%s" % suspect.from_domain)


#class DKIMTestCases(unittest.TestCase):
# todo


class DMARCTests(unittest.TestCase):
    def test_dmarc(self):
        self.assertTrue(DMARC_AVAILABLE)
        import dmarc
        dmarc_records = [
            "v=DMARC1; p=reject; adkim=s; aspf=s; rua=mailto:dmarc@example.com; ruf=mailto:dmarc@example.com; rf=afrf; pct=100;",
            "v=DMARC1; p=none; sp=quarantine; rua=mailto:dmarc@example.com",
            "v=DMARC1; p=reject; rua=mailto:dmarc@example.com"
        ]
        
        header_from_domain = 'example.com'
        
        config = FuConfigParser()
        dmarcplugin = DMARCPlugin(config=config)
        for dmarc_record in dmarc_records:
            aspf = dmarcplugin._mk_aspf(None, None)
            adkim = dmarcplugin._mk_adkim(None, None, None)
            result, dispo = dmarcplugin._do_dmarc_check(dmarc_record, header_from_domain, aspf, adkim, 'id')
            self.assertEqual(result, dmarc.POLICY_FAIL)




class DKIMDummy(DKIM):
    verified = False
    def verify(self, idx=0, dnsfunc=None):
        return self.verified


class DKIMTests(unittest.TestCase):
    """
    test that we get the proper return values (valid, validauthor, validsender, invalid).
    this test does not verify the functionality of dkim library!
    """
    def setUp(self):
        config = FuConfigParser()
        self.dkimplugin = DKIMVerifyPlugin(config=config)
        self.dkimplugin.DKIM = DKIMDummy
    
    
    def mk_maildummy(self, hdr_from_addr, dkim_dom):
        msg = MIMEMultipart()
        msg['From'] = hdr_from_addr
        msg['DKIM-Signature'] = "v=1; a=rsa-sha256; c=relaxed/relaxed; d=%s; h=from : to : subject : date : message-id : references : in-reply-to : content-type : mime-version; s=pps0720; bh=+9R85jf8sq4ukzQRIjPXRj6UKRTf+RsKBi967f1WyZs=; b=N3qU7YgZkj8JyDS3gbmRqCVqG5AWWb8nautbsS7oY/h9J/IGtbBDT8kAkTlhbUf+ZoynwFxRvQFwUWuWoPTlqhe+mcDzg7vjp8TXx8xnVlZtlCjRdICN20hd1bTl2m3iXayZsEsFiGV7FmduoaYclZp/NEWAgMxzIk1tsg5SIXYEwayF6Qety681bwatrsmWe2Og5kisWRK5Qnt0j5m/HFnvI31bL42wxFzCYEf7ph+0HFZQTc+/3d336fEoaXNnHohWmtf5RhsS5URVl1/nG0csNnT58Fu/HVuo2iMzKxdGII6/gKIxWTyZBGoe+UWMToPGqPSGxch62400p86EvA==" % dkim_dom
        return msg
    
    
    def test_dkim_valid(self):
        self.dkimplugin.DKIM.verified = True
        tests = [
            {'hdr_from_addr': 'user@example.org', 'dkim_dom': 'example.org', 'env_from_addr': 'user@example.org', 'result': DKIM_VALID_AUTHOR},
            {'hdr_from_addr': 'user@example.com', 'dkim_dom': 'example.org', 'env_from_addr': 'user@example.org', 'result': DKIM_VALID_SENDER},
            {'hdr_from_addr': 'user@example.com', 'dkim_dom': 'example.org', 'env_from_addr': 'user@example.com', 'result': DKIM_VALID},
        ]
        
        for test in tests:
            msg = self.mk_maildummy(test.get('hdr_from_addr'), test.get('dkim_dom'))
            suspect = Suspect(test.get('env_from_addr'), 'recipient@unittests.fuglu.org', "/dev/null")
            suspect.set_message_rep(msgrep=msg)
            
            self.dkimplugin.examine(suspect)
            valid = suspect.get_tag("DKIMVerify.result")
            self.assertEqual(valid, test.get('result'))
    
    
    def test_dkim_invalid(self):
        self.dkimplugin.DKIM.verified = False
        msg = self.mk_maildummy('user@example.org', 'example.org')
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', "/dev/null")
        suspect.set_message_rep(msgrep=msg)
        self.dkimplugin.examine(suspect)
        valid = suspect.get_tag("DKIMVerify.result")
        self.assertEqual(valid, DKIM_INVALID)


class TestSPFOut(unittest.TestCase):
    def test_given_vals(self):
        """Test ip/hostname used if both are given directly"""
        config = FuConfigParser()
        config.add_section("SPFOut")
        config.set("SPFOut", "ip", "192.168.1.1")
        config.set("SPFOut", "hostname", "testing.localhost")

        spfout = SPFOut(config=config)
        spfout.lint()
        self.assertEqual("192.168.1.1", spfout._myip)
        self.assertEqual("testing.localhost", spfout._myhostname)
        self.assertEqual("testing.localhost", spfout._myhelo)

    def test_given_vals_in_env(self):
        """Test ip/hostname used if both are given by environment variables"""
        config = FuConfigParser()
        config.add_section("SPFOut")

        ip_envname = "X-TMP-FUGLU-TESTING-SPFOUT-IP-ENV"
        hn_envname = "X-TMP-FUGLU-TESTING-SPFOUT-HN-ENV"

        os.environ[ip_envname] = "192.168.1.1"
        os.environ[hn_envname] = "testing.localhost"

        config.set("SPFOut", "ip", f"${ip_envname}")
        config.set("SPFOut", "hostname", f"${hn_envname}")

        spfout = SPFOut(config=config)
        spfout.lint()

        self.assertEqual("192.168.1.1", spfout._myip)
        self.assertEqual("testing.localhost", spfout._myhostname)
        self.assertEqual("testing.localhost", spfout._myhelo)
