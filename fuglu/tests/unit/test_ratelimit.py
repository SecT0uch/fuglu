# -*- coding: UTF-8 -*-
import unittest
import logging
import yaml
import ipaddress
import asyncio
import os
from configparser import ConfigParser

from fuglu.plugins.ratelimit.main import Limiter, CounterInt, MilterSessionAttr, CounterSuspectTag, RateLimitPlugin
from fuglu.shared import Suspect, DEFER, REJECT, DUNNO, actioncode_to_string
import fuglu.connectors.milterconnector as sm
import fuglu.connectors.asyncmilterconnector as asm
from unittest.mock import patch, MagicMock
from fuglu.connectors.milterconnector import (
    CONNECT, HELO, MAILFROM,
    RCPT, HEADER, EOH, EOB
)


class PrintTitleMixin:
    """Mixin for setup - print title, setup logger config"""
    backend = ""

    def _print_title(self, title: str):
        title_line = f"- ({self.backend}) {title} -"
        title_dash = len(title_line)*'-'
        print(f"\n{title_dash}\n{title_line}\n{title_dash}\n")

        logging.basicConfig(level=logging.DEBUG)


class TestRule(unittest.TestCase):
    def test_ratestring(self):
        """Test string for rate"""
        number = 2
        frame = 4.4
        parsed_number, parsed_frame = Limiter._parse_rate(f"{number}/{frame}")
        self.assertEqual(parsed_number, number)
        self.assertEqual(parsed_frame, frame)

    def test_ratedictstring(self):
        """Test dict for rate"""
        number = 2
        frame = 4.4
        parsed_number, parsed_frame = Limiter._parse_rate({'number': f"{number}",
                                                           'frame': f"{frame}"})
        self.assertEqual(parsed_number, number)
        self.assertEqual(parsed_frame, frame)

    def test_prefix_parse(self):
        """Test prefix parsing"""
        number = 10.1
        frame = 10.2
        name = "bliblablubb"
        message = "palim palim"
        out = Limiter._parse_prefix({
            'rate': f"{number}/{frame}",
            'name': name,
            'message': message
        })
        self.assertEqual((name, number, frame, message, None, None), out)

    def test_prefix_parse_env(self):
        """Test prefix parsing using name fron env var"""
        number = 10.1
        frame = 10.2
        name = "bliblablubb"
        envname = "X-TMP-FUGLU-TESTING-PREFIX-FROM-ENV"
        os.environ[envname] = name
        message = "palim palim"
        out = Limiter._parse_prefix({
            'rate': f"{number}/{frame}",
            'name': f"${envname}",
            'message': message
        })
        self.assertEqual((name, number, frame, message, None, None), out)

    def test_regex_string(self):
        """Test regex strings"""
        reg = CounterInt._parse_regex(r"^.*\@domain.invalid$")
        self.assertIs(1, len(reg))

    def test_regex_stringlist(self):
        """Test list of regex strings"""
        reg = CounterInt._parse_regex([r"^.*\@domain.invalid$", r"^.*\@domain.invalid$"])
        self.assertIs(2, len(reg))

    def test_regex_string_error(self):
        """Test if regex with error raises an exception"""
        with self.assertRaises(Exception):
            _ = CounterInt._parse_regex(r"(")

    def test_noregex(self):
        """Without regex defined for the limiter every call should be a hit"""
        setupdict = yaml.safe_load("""
dummyfromrule2:
    rate: 5/10
    action: DUNNO    
    count:
        from_address:
            type: MilterSession
        """)
        r = Limiter(name="test", setupdict=setupdict["dummyfromrule2"])

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.sender = 'sender@domain.invalid'
        suspect.recipients = ['recipient@domain.invalid']
        self.assertTrue(r.examine(suspect=suspect))

        # any suspect will match limiter condition
        suspect.sender = 'sender1@domain.invalid'
        self.assertTrue(r.examine(suspect=suspect))

    def test_loadsingleregex(self):
        """Check single regex match for limiter"""
        setupdict = yaml.safe_load("""
dummyfromrule2:
    rate: 5/10
    action: DUNNO    
    count:
        from_address:
            type: Suspect
            regex: ^.*@domain.invalid$
        """)
        r = Limiter(name="test", setupdict=setupdict["dummyfromrule2"])

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.sender = 'sender@domain.invalid'
        suspect.recipients = ['recipient@domain.invalid']

        self.assertTrue(r.examine(suspect=suspect))

        # create suspect which should NOT match limiter conditions
        suspect.sender = 'sender@another.domain.invalid'
        self.assertFalse(r.examine(suspect=suspect))

    def test_loadmultiregex(self):
        """Check any regex match for limiter"""
        setupdict = yaml.safe_load("""
dummyfromrule2:
    rate: 5/10
    action: DUNNO    
    count:
        from_address:
            type: Suspect
            regex:
                - ^.*@domain.invalid$
                - ^.*@another.domain.invalid$
        """)
        r = Limiter(name="test", setupdict=setupdict["dummyfromrule2"])

        # create suspect which should match first limiter regex condition
        suspect = sm.MilterSession(None)
        suspect.sender = 'sender@domain.invalid'
        suspect.recipients = ['recipient@domain.invalid']
        self.assertTrue(r.examine(suspect=suspect))

        # create suspect which should match second limiter regex condition
        suspect.sender = 'sender@another.domain.invalid'
        self.assertTrue(r.examine(suspect=suspect))

        # create suspect which should NOT match limiter conditions
        suspect.sender = 'sender@disabled.domain.invalid'
        self.assertFalse(r.examine(suspect=suspect))

    def test_net_string(self):
        """Test network strings"""
        nets = CounterInt._parse_ipmatch(r"192.168.1.1/16")
        self.assertIs(1, len(nets))

    def test_net_stringlist(self):
        """Test list of regex strings"""
        nets = CounterInt._parse_ipmatch([r"192.168.1.1/24", r"192.168.2.1/24"])
        self.assertIs(2, len(nets))

    def test_net_string_error(self):
        """Test if regex with error raises an exception"""
        with self.assertRaises(Exception):
            _ = CounterInt._parse_ipmatch(r"192.168")

    def test_loadsingleipmatch(self):
        """Check single ipmatch for limiter"""
        setupdict = yaml.safe_load("""
dummyfromrule2:
    rate: 5/10
    action: DUNNO    
    count:
        addr:
            type: MilterSession
            ipmatch: 192.168.1.1/24
        """)
        r = Limiter(name="test", setupdict=setupdict["dummyfromrule2"])
        # create suspect which should match limiter conditions

        suspect = sm.MilterSession(None)
        suspect.sender = 'sender@domain.invalid'
        suspect.recipients = ['recipient@domain.invalid']
        suspect.addr = '192.168.1.1'
        self.assertTrue(r.examine(suspect=suspect))

        # create suspect which should NOT match limiter conditions
        suspect.addr = '192.168.2.1'
        self.assertFalse(r.examine(suspect=suspect))

    def test_loadmultiipmatch(self):
        """Check any regex match for limiter"""
        setupdict = yaml.safe_load("""
dummyfromrule2:
    rate: 5/10
    action: DUNNO    
    count:
        addr:
            type: MilterSession
            ipmatch:
                - 192.168.1.0/24
                - 192.168.2.0/24
        """)
        r = Limiter(name="test", setupdict=setupdict["dummyfromrule2"])

        # create suspect which should match first limiter regex condition
        suspect = sm.MilterSession(None)
        suspect.sender = 'sender@domain.invalid'
        suspect.recipients = ['recipient@domain.invalid']
        suspect.addr = '192.168.1.1'
        self.assertTrue(r.examine(suspect=suspect))

        # create suspect which should match second limiter regex condition
        suspect.sender = 'sender@another.domain.invalid'
        suspect.addr = '192.168.2.1'
        self.assertTrue(r.examine(suspect=suspect))

        # create suspect which should NOT match limiter conditions
        suspect.sender = 'sender@disabled.domain.invalid'
        suspect.addr = '192.168.3.1'
        self.assertFalse(r.examine(suspect=suspect))

    def test_stringlist_string(self):
        mystring = "abc"
        self.assertEqual([mystring], CounterInt.parse_option_stringlist(mystring))

    def test_stringlist_stringlist(self):
        mystringlist = ["abc", "def"]
        self.assertEqual(mystringlist, CounterInt.parse_option_stringlist(mystringlist))

    def test_keyfuncloader(self):
        """Playground"""
        #from ratelimit.helperfuncs import findreturn, function_from_string
        #function_from_string("<func> bla(a=b,d=d)[10]")
        #function_from_string("<func> bla()[11]")
        #function_from_string("<func> bla[12]")
        #function_from_string("<func> .bla[12]")
        #function_from_string("<func> bla(a=b,i=(int)1,f=(float)1.1)[10][3]")

        from fuglu.plugins.ratelimit.dynfunction import FunctionWrapper, MultipleFunctionsWrapper
        #myfunc = FunctionWrapper("bla(a=b,i=(int)1,f=(float)1.1)[10][3]")
        myfunc = FunctionWrapper("bla(a=,b=5)")  # pass keyword argument from input dict,
                                                 # use 'b=5' from definition
        myfunc(a=10)
        #multifuncs = MultipleFunctionsWrapper(['<func> toint', '<func> isint'])

        #print(multifuncs('2'))

    # <name>:
    #     ...
    #     count:
    #         <fieldname>:
    #             ...
    #             key: <keyname> # define a common key for counter (otherwise field value is used)
    #                            # or function as "<func> module.functionname" or as an array of functions
    #                            # which is applied one after the other where the output is passed as argument
    #                            # Note <keyname> can contain template vars, for example "${heloname}"
    #                 - <func> module1.function1
    #                 - <func> module2.function2[myfactor=5] # square brackets can be used to pass additional keyword args

    def test_key(self):
        setupdict = yaml.safe_load("""
from_address:
   type: MilterSession
   regex: ^.*@domain.invalid$
   key: fill(${to_domain})this
""")
        counter = MilterSessionAttr(fieldname="from_address", setupdict=setupdict["from_address"])

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.sender = 'sender@domain.invalid'
        suspect.recipients = ['recipient@domain.invalid']

        key = counter.get_key(suspect=suspect)
        print(f"key(original): {counter.key}")
        print(f"key(processed): {key}")
        self.assertEqual("fill(domain.invalid)this",key)

    def test_key_subnet(self):
        setupdict = yaml.safe_load("""
from_address:
   type: MilterSession
   regex: ^.*@domain.invalid$
   key: 
       - ${addr}
       - <func> fuglu.plugins.ratelimit.helperfuncs.ip2network(prefixlen=(int)24)
""")
        counter = MilterSessionAttr(fieldname="from_address", setupdict=setupdict["from_address"])

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.sender = 'sender@domain.invalid'
        suspect.recipients = ['recipient@domain.invalid']
        suspect.addr = '192.168.100.32'

        key = counter.get_key(suspect=suspect)
        print(f"key(original): {counter.key}")
        print(f"key(processed): {key}")
        self.assertEqual("192.168.100.0/24", key)

    def test_key_subnet_as_string(self):
        setupdict = yaml.safe_load("""
    from_address:
       type: MilterSession
       regex: ^.*@domain.invalid$
       key: 
           - ${addr}
           - <func> fuglu.plugins.ratelimit.helperfuncs.ip2network(prefixlen=(int)24)
           - <func> fuglu.stringencode.force_uString(convert_none=(bool)True)
    """)
        counter = MilterSessionAttr(fieldname="from_address", setupdict=setupdict["from_address"])

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.sender = 'sender@domain.invalid'
        suspect.recipients = ['recipient@domain.invalid']
        suspect.addr = '192.168.100.32'

        key = counter.get_key(suspect=suspect)
        print(f"key(original): {counter.key}")
        print(f"key(processed): {key}")
        self.assertEqual("192.168.100.0/24", key)

    def test_key_extractdomain(self):
        """Test domain extraction"""
        setupdict = yaml.safe_load("""
sndrdomain4recipientdomain:
   type: MilterSession
   regex: ^.*@domain.invalid$
   key:
       - ${from_domain}
       - <func> fuglu.plugins.ratelimit.helperfuncs.get_domain_from_uri(suspect=)
""")
        counter = MilterSessionAttr(fieldname="from_address", setupdict=setupdict["sndrdomain4recipientdomain"])

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.sender = 'sender@invalid.fuglu.org'
        suspect.recipients = ['recipient@domain.invalid']

        key = counter.get_key(suspect=suspect)
        print(f"key(original): {counter.key}")
        print(f"key(processed): {key}")
        self.assertEqual("fuglu.org", key)

    def _get_reader_mock(self):
        # mock answer -> Reader -> asn -> response object
        readermock = MagicMock()
        returnmock = MagicMock()
        returnmock.autonomous_system_number = -1
        returnmock.autonomous_system_organization = "Dummy"
        returnmock.ip_address = "192.168.1.100"
        returnmock.network = ipaddress.IPv4Network("192.168.1.0/24")

        readermock.asn.return_value = returnmock
        return readermock

    @patch('geoip2.database.Reader')
    def test_asn_extractdomain(self, geoipreader):
        """Test domain extraction"""

        geoipreader.return_value = self._get_reader_mock()

        setupdict = yaml.safe_load("""
test:
   type: MilterSession
   key:
       - ${addr}
       - <func> fuglu.plugins.ratelimit.helperfuncs.asn(geoipfilename=/path/to/datafile/filename.extension)[1]
""")
        counter = MilterSessionAttr(fieldname="from_address", setupdict=setupdict["test"])

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.sender = 'sender@invalid.fuglu.org'
        suspect.recipients = ['recipient@domain.invalid']
        suspect.addr = '192.168.1.100'

        key = counter.get_key(suspect=suspect)
        print(f"input address: {suspect.addr}")
        print(f"key(original): {counter.key}")
        print(f"key(processed): {key}")
        self.assertIn('dummy', key.lower())

    @patch('geoip2.database.Reader')
    def test_asn_org_extractdomain(self, geoipreader):
        """Test domain extraction"""
        setupdict = yaml.safe_load("""
test:
   type: MilterSession
   key:
       - ${addr}
       - <func> fuglu.plugins.ratelimit.helperfuncs.asn(geoipfilename=/path/to/datafile/filename.extension)[0]
       - <func> fuglu.stringencode.force_uString(convert_none=(bool)True)
""")
        counter = MilterSessionAttr(fieldname="from_address", setupdict=setupdict["test"])

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.sender = 'sender@invalid.fuglu.org'
        suspect.recipients = ['recipient@domain.invalid']
        suspect.addr = '192.168.1.100'

        geoipreader.return_value = self._get_reader_mock()

        key = counter.get_key(suspect=suspect)
        print(f"input address: {suspect.addr}")
        print(f"key(original): {counter.key}")
        print(f"key(processed): {key}")
        self.assertEqual('-1', key)

    def test_field_userdef(self):
        """Test domain extraction"""
        setupdict = yaml.safe_load("""
test:
   type: MilterSession
   field: palimpalim
""")
        counter = MilterSessionAttr(fieldname="from_address", setupdict=setupdict["test"])

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.sender = 'sender@invalid.fuglu.org'
        suspect.recipients = ['recipient@domain.invalid']
        suspect.addr = '192.168.1.100'

        field = counter.get_field(suspect=suspect)
        print(f"field(original): {counter.field}")
        print(f"field(processed): {field}")
        self.assertEqual('palimpalim', field)

    @patch('geoip2.database.Reader')
    def test_asn_org_regexmatch(self, geoipreader):
        """Test domain extraction"""
        setupdict = yaml.safe_load("""
addr:
   type: MilterSession
   field:
       - <func> fuglu.plugins.ratelimit.helperfuncs.asn(geoipfilename=/path/to/datafile/filename.extension)[0]
       - <func> fuglu.stringencode.force_uString(convert_none=(bool)True)
   regex: ^-1$
""")
        counter = MilterSessionAttr(fieldname="addr", setupdict=setupdict["addr"])

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.sender = 'sender@invalid.fuglu.org'
        suspect.recipients = ['recipient@domain.invalid']
        suspect.addr = '192.168.1.100'

        geoipreader.return_value = self._get_reader_mock()

        field = counter.get_field(suspect=suspect)
        key = counter.get_key(suspect=suspect)
        counter_applies = counter.examine(suspect=suspect)
        print(f"input address: {suspect.addr}")
        print(f"key(original): {counter.key}")
        print(f"key(processed): {key}")
        print(f"field(original): {counter.field}")
        print(f"field(processed): {field}")
        print(f"counter applies (regex match): {counter_applies}")
        self.assertEqual('-1', key)
        self.assertEqual('-1', field)
        self.assertTrue(counter_applies)

    @patch('geoip2.database.Reader')
    def test_asn_org_no_regexmatch(self, geoipreader):
        """Test domain extraction"""
        setupdict = yaml.safe_load("""
addr:
   type: MilterSession
   field:
       - <func> fuglu.plugins.ratelimit.helperfuncs.asn(geoipfilename=/path/to/datafile/filename.extension)[0]
       - <func> fuglu.stringencode.force_uString(convert_none=(bool)True)
   regex: ^9999$
""")
        counter = MilterSessionAttr(fieldname="from_address", setupdict=setupdict["addr"])

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.sender = 'sender@invalid.fuglu.org'
        suspect.recipients = ['recipient@domain.invalid']
        suspect.addr = '192.168.1.100'

        geoipreader.return_value = self._get_reader_mock()

        field = counter.get_field(suspect=suspect)
        key = counter.get_key(suspect=suspect)
        counter_applies = counter.examine(suspect=suspect)
        print(f"input address: {suspect.addr}")
        print(f"key(original): {counter.key}")
        print(f"key(processed): {key}")
        print(f"field(original): {counter.field}")
        print(f"field(processed): {field}")
        print(f"counter applies (regex match): {counter_applies}")
        self.assertEqual('-1', key)
        self.assertEqual('-1', field)
        self.assertFalse(counter_applies, "This regex shouldn't match")

    def test_anyregexmatch(self):
        """Check any regex match for limiter"""
        setupdict = yaml.safe_load("""
testrule:
    rate: 5/10
    action: DUNNO    
    count:
        addr:
            type: MilterSession
            ipmatch:
                - 192.168.1.0/24
                - 192.168.2.0/24
        """)
        r = Limiter(name="test", setupdict=setupdict["testrule"])

        # create suspect which should match first limiter regex condition
        suspect = sm.MilterSession(None)
        suspect.sender = 'sender@domain.invalid'
        suspect.recipients = ['recipient@domain.invalid']
        suspect.addr = '192.168.1.1'
        self.assertTrue(r.examine(suspect=suspect))

        # create suspect which should match second limiter regex condition
        suspect.sender = 'sender@another.domain.invalid'
        suspect.addr = '192.168.2.1'
        self.assertTrue(r.examine(suspect=suspect))

        # create suspect which should NOT match limiter conditions
        suspect.sender = 'sender@disabled.domain.invalid'
        suspect.addr = '192.168.3.1'
        self.assertFalse(r.examine(suspect=suspect))

    def test_search_in_array(self):
        """apply regex on simple template"""
        setupdict = yaml.safe_load("""
custom:
    type: SuspectTag
    field:
        - <func> fuglu.plugins.ratelimit.helperfuncs.match_key_in_array(searchstring=palim@fuglu.org,suspect=)
  """)
        counter = CounterSuspectTag(fieldname="emails", setupdict=setupdict["custom"])

        suspect = Suspect("from@fuglu.org", "to@fuglu.org", None, inbuffer=b"empty")
        counter_applies = counter.examine(suspect=suspect)
        self.assertFalse(counter_applies)

        suspect.set_tag('emails', ["palim@fuglu.org", "palim1@fuglu.org"])
        counter_applies = counter.examine(suspect=suspect)
        self.assertTrue(counter_applies)


    def test_uri_search_in_array(self):
        """Search in body uris array"""
        setupdict = yaml.safe_load("""
custom:
    type: SuspectTag
    field:
        - <func> fuglu.plugins.ratelimit.helperfuncs.valid_fqdn(suspect=)
        - <func> fuglu.plugins.ratelimit.helperfuncs.match_key_in_array(searchstring=fuglu.org,suspect=)
  """)
        counter = CounterSuspectTag(fieldname="body.uris", setupdict=setupdict["custom"])

        suspect = Suspect("from@fuglu.org", "to@fuglu.org", None, inbuffer=b"empty")
        counter_applies = counter.examine(suspect=suspect)
        self.assertFalse(counter_applies)

        suspect.set_tag('body.uris', ["https://fuglu.org/palim", "https://domain.invali/palim"])
        counter_applies = counter.examine(suspect=suspect)
        self.assertTrue(counter_applies)

    def test_truefalse(self):
        """Test true/false helper"""
        setupdict = yaml.safe_load("""
custom:
    type: MilterSession
    field:
        - <func> fuglu.plugins.ratelimit.helperfuncs.convert_truefalse(suspect=)
  """)
        counter = MilterSessionAttr(fieldname="tls_version", setupdict=setupdict["custom"])

        suspect = asm.MilterSession(MagicMock(), MagicMock())
        counter_applies = counter.examine(suspect=suspect)
        self.assertTrue(counter_applies)

        self.assertEqual("false", counter.get_field(suspect=suspect).lower())

        suspect.tls_version = "1.0"

        self.assertEqual("true", counter.get_field(suspect=suspect).lower())

        counter.lint()


class TestCounter(unittest.TestCase):

    def test_lint_ipmatch(self):

        setupdict = yaml.safe_load("""
test:
    type: MilterSession
    ipmatch:
        - 192.168.1.0/24
    lint:
        - input: 192.168.1.1
          examine: True
        - input: 192.168.1.3
          examine: True
        - input: 192.168.2.3
          examine: False
          """)
        counter = MilterSessionAttr(fieldname="from_address", setupdict=setupdict["test"])
        self.assertEqual(0, counter.lint())

    def test_lint_badinput(self):

        setupdict = yaml.safe_load("""
test:
    type: MilterSession
    ipmatch:
        - 192.168.1.0/24
    lint:
        - input: 192.168.1.1
  """)
        counter = MilterSessionAttr(fieldname="from_address", setupdict=setupdict["test"])
        self.assertEqual(1, counter.lint())

    def test_lint_simpletemplate(self):
        """apply regex on simple template"""
        setupdict = yaml.safe_load("""
custom:
    type: MilterSession
    field:
        - ${heloname}
        - <func> fuglu.plugins.ratelimit.helperfuncs.split_helo2host_domain(suspect=)[0]
    regex: ^[0-9\.]+$
    lint:
        - input: 192.168.0.1.fuglu.org
          examine: True
        - input: bladomain.fuglu.org
          examine: False
  """)
        counter = MilterSessionAttr(fieldname="from_address", setupdict=setupdict["custom"])
        self.assertEqual(0, counter.lint())

    def test_lint_inputdict(self):
        """Test lint input dictionary for more complexe template inputs"""
        setupdict = yaml.safe_load("""
custom:
    type: MilterSession
    field:
        - ${heloname}//${to_address}
    regex: ^[0-9\.]+fuglu.org\/\/.*\@domain\.invalid$
    lint:
        - templatedict: 
              heloname: 192.168.0.1.fuglu.org
              to_address: unknown@domain.invalid
          examine: True
        - templatedict: 
              heloname: 192.168.0.1.fuglu.org
              to_address: unknown@domain
          examine: False
        - templatedict: 
              heloname: bladomain.fuglu.org
              to_address: unknown@domain.invalid
          examine: False
  """)
        counter = MilterSessionAttr(fieldname="from_address", setupdict=setupdict["custom"])
        self.assertEqual(0, counter.lint())

    def test_lint_fieldout(self):
        """lint for fieldout"""
        setupdict = yaml.safe_load("""
        custom:
            type: MilterSession
            field:
                - ${heloname}//${to_address}
            lint:
                - templatedict: 
                      heloname: 192.168.0.1.fuglu.org
                      to_address: unknown@domain.invalid
                  fieldout: 192.168.0.1.fuglu.org//unknown@domain.invalid
          """)
        counter = MilterSessionAttr(fieldname="from_address", setupdict=setupdict["custom"])
        self.assertEqual(0, counter.lint())

    def test_lint_fieldout_fail(self):
        """This lint for fieldout is expected to fail"""
        setupdict = yaml.safe_load("""
        custom:
            type: MilterSession
            field:
                - ${heloname}//${to_address}
            lint:
                - templatedict: 
                      heloname: 192.168.0.1.fuglu.org
                      to_address: unknown@domain.invalid
                  fieldout: 192.168.0.1.fuglu.org//unknown@domain
          """)
        counter = MilterSessionAttr(fieldname="from_address", setupdict=setupdict["custom"])
        self.assertEqual(1, counter.lint())

    def test_lint_keyout(self):
        """lint for keyout"""
        setupdict = yaml.safe_load("""
        custom:
            type: MilterSession
            key:
                - ${heloname}//${to_address}
            lint:
                - templatedict: 
                      heloname: 192.168.0.1.fuglu.org
                      to_address: unknown@domain.invalid
                  keyout: 192.168.0.1.fuglu.org//unknown@domain.invalid
          """)
        counter = MilterSessionAttr(fieldname="from_address", setupdict=setupdict["custom"])
        self.assertEqual(0, counter.lint())

    def test_lint_keyout_fail(self):
        """This lint for keyout is expected to fail"""
        setupdict = yaml.safe_load("""
        custom:
            type: MilterSession
            key:
                - ${heloname}//${to_address}
            lint:
                - templatedict: 
                      heloname: 192.168.0.1.fuglu.org
                      to_address: unknown@domain.invalid
                  keyout: 192.168.0.1.fuglu.org//unknown@domain
          """)
        counter = MilterSessionAttr(fieldname="from_address", setupdict=setupdict["custom"])
        self.assertEqual(1, counter.lint())

    def test_lint_unknowniput(self):
        """Lint is expected to fail due to unknown keys"""
        setupdict = yaml.safe_load("""
        custom:
            type: MilterSession
            key:
                - palim
            lint:
                - input: palim
                  keyout: palim
                  bla: bla
          """)
        counter = MilterSessionAttr(fieldname="from_address", setupdict=setupdict["custom"])
        self.assertEqual(1, counter.lint())

    def test_lint_unknownfield(self):
        """Lint is expected to fail due to unknown keys"""
        setupdict = yaml.safe_load("""
        custom:
            type: MilterSession
            key:
                - palim
            lint:
                - input: palim
                  keyout: palim
          """)
        counter = MilterSessionAttr(fieldname="nonexisting_field", setupdict=setupdict["custom"])
        self.assertEqual(1, counter.lint())

    def test_lint_unknownfield_overwrite(self):
        """Lint is expected to fail due to unknown keys"""
        setupdict = yaml.safe_load("""
        custom:
            type: MilterSession
            field:
                - pffff
            key:
                - palim
            lint:
                - input: palim
                  keyout: palim
                  fieldout: pffff
          """)
        counter = MilterSessionAttr(fieldname="nonexisting_field", setupdict=setupdict["custom"])
        self.assertEqual(0, counter.lint())

    def test_lint_search_in_array(self):
        """apply regex on simple template"""
        setupdict = yaml.safe_load("""
custom:
    type: SuspectTag
    field:
        - <func> fuglu.plugins.ratelimit.helperfuncs.match_key_in_array(searchstring=palim@fuglu.org,suspect=)
    lint:
        - input: 
            - palim@fuglu.org
            - palim1@fuglu.org
          examine: True
        - input: 
            - palim1@fuglu.org
            - palim2@fuglu.org
          examine: False
  """)
        counter = CounterSuspectTag(fieldname="emails", setupdict=setupdict["custom"])
        self.assertEqual(0, counter.lint())

    def test_lint_search_uri_in_array(self):
        """apply regex on simple template"""
        setupdict = yaml.safe_load("""
custom:
    type: SuspectTag
    field:
        - <func> fuglu.plugins.ratelimit.helperfuncs.valid_fqdn(suspect=)
        - <func> fuglu.plugins.ratelimit.helperfuncs.match_key_in_array(searchstring=fuglu.org,suspect=)
    lint:
        - input: 
            - https://fuglu.org/palim
            - https://domain.invalid/palim
          examine: True
        - input: 
            - https://domain.invalid/palim1
            - https://domain.invalid/palim2
          examine: False
  """)
        counter = CounterSuspectTag(fieldname="emails", setupdict=setupdict["custom"])
        self.assertEqual(0, counter.lint())

    def test_truefalse_lint(self):
        """Test true/false helper"""
        setupdict = yaml.safe_load("""
custom:
    type: MilterSession
    field:
        - <func> fuglu.plugins.ratelimit.helperfuncs.convert_truefalse(suspect=)
    lint:
        - input: 1.0    
          fieldout: "True"
        - input: ""
          fieldout: "False"
  """)
        counter = MilterSessionAttr(fieldname="tls_version", setupdict=setupdict["custom"])
        self.assertEqual(0, counter.lint())


class TestFull(unittest.TestCase):
    def test_specialaction(self):
        """Test different actions, default & bounceaction"""
        confdefaults = """
[test]        
limiterfile=/etc/fuglu/ratelimit.yml'
backendtype=memory
backendconfig=
state=rcpt
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
dummyfromrule2:
    strategy: always-hit
    rate: 1/1
    state: rcpt
    action: DEFER    
    bounceaction: REJECT
    count:
        addr:
            type: MilterSession
""")
        with patch.object(RateLimitPlugin, 'yamlfile2dict', return_value=setupdict):
            rplugin = RateLimitPlugin(config=conf)

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.addr = "192.168.1.1"
        suspect.sender = ''
        suspect.recipients = ['recipient@domain.invalid']

        res = asyncio.run(rplugin.core(suspect=suspect, state=RCPT))
        print(res)
        self.assertEqual(res[0], REJECT)

        # any suspect will match limiter condition
        suspect.sender = 'sender1@domain.invalid'
        res = asyncio.run(rplugin.core(suspect=suspect, state=RCPT))
        print(res)
        self.assertEqual(res[0], DEFER)

    def test_prefix(self):
        """Test prefix"""
        confdefaults = """
[test]        
limiterfile=/etc/fuglu/ratelimit.yml'
backendtype=memory
backendconfig=
state=rcpt
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
dummyfromrule2:
    strategy: always-hit
    rate: 1/1
    state: rcpt
    action: DEFER    
    message: original
    prefix: 
        name: palim
        rate: 10/1
        message: prefixed
        count:
            addr:
                type: MilterSession
                ipmatch: 192.168.1.1
                invert: False
    count:
        addr:
            type: MilterSession
            ipmatch: 192.168.1.1
            invert: True
""")
        with patch.object(RateLimitPlugin, 'yamlfile2dict', return_value=setupdict):
            rplugin = RateLimitPlugin(config=conf)

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.addr = "192.168.2.1"
        suspect.sender = ''
        suspect.recipients = ['recipient@domain.invalid']

        res = asyncio.run(rplugin.core(suspect=suspect, state=RCPT))
        print(res)
        self.assertEqual(res, (DEFER, "original"), "original non-prefixed rule matches first")

        # any suspect will match limiter condition
        suspect.addr = "192.168.1.1"
        res = asyncio.run(rplugin.core(suspect=suspect, state=RCPT))
        print(res)
        self.assertEqual(res, (DEFER, "prefixed"), "original rule doesn't match, but prefixed one")

    def test_prefix_action(self):
        """Test prefix action and order"""
        confdefaults = """
[test]        
limiterfile=/etc/fuglu/ratelimit.yml'
backendtype=memory
backendconfig=
state=rcpt
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
dummyfromrule2:
    strategy: always-hit
    rate: 1/1
    state: rcpt
    action: DEFER    
    message: original
    prefix: 
        name: palim
        message: prefixed
        action: REJECT
        count:
            addr:
                type: MilterSession
                ipmatch: 192.168.1.1
    count:
        addr:
            type: MilterSession
            ipmatch: 192.168.1.0/24
""")
        with patch.object(RateLimitPlugin, 'yamlfile2dict', return_value=setupdict):
            rplugin = RateLimitPlugin(config=conf)

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.addr = "192.168.1.2"
        suspect.sender = ''
        suspect.recipients = ['recipient@domain.invalid']

        res = asyncio.run(rplugin.core(suspect=suspect, state=RCPT))
        print(res)
        self.assertEqual(res, (DEFER, "original"), "original non-prefixed rule matches first")

        # any suspect will match limiter condition
        suspect.addr = "192.168.1.1"
        res = asyncio.run(rplugin.core(suspect=suspect, state=RCPT))
        print(res)
        self.assertEqual(res, (REJECT, "prefixed"), "original rule doesn't match, but prefixed one")

    def test_multiple(self):
        """Test different counters, both have to match for a hit"""
        confdefaults = """
[test]        
limiterfile=/etc/fuglu/ratelimit.yml'
backendtype=memory
backendconfig=
state=rcpt
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
dummyfromrule2:
    strategy: always-hit
    rate: 1/1
    state: rcpt
    action: DEFER    
    count:
        addr:
            type: MilterSession
            ipmatch: 192.168.1.1
        from_address:
            type: MilterSession
            regex: ^sender1@domain\.invalid$
""")
        with patch.object(RateLimitPlugin, 'yamlfile2dict', return_value=setupdict):
            rplugin = RateLimitPlugin(config=conf)

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.addr = "192.168.1.1"
        suspect.sender = ''
        suspect.recipients = ['recipient@domain.invalid']

        res = asyncio.run(rplugin.core(suspect=suspect, state=RCPT))
        print(res)
        returncode = res[0] if isinstance(res, tuple) else res
        self.assertEqual(returncode, DUNNO)

        # any suspect will match limiter condition
        suspect.sender = 'sender1@domain.invalid'
        res = asyncio.run(rplugin.core(suspect=suspect, state=RCPT))
        print(res)
        returncode = res[0] if isinstance(res, tuple) else res
        self.assertEqual(returncode, DEFER, f"Response is {actioncode_to_string(returncode)}")

        # any suspect will match limiter condition
        suspect.addr = "192.168.1.2"
        suspect.sender = 'sender1@domain.invalid'
        res = asyncio.run(rplugin.core(suspect=suspect, state=RCPT))
        print(res)
        returncode = res[0] if isinstance(res, tuple) else res
        self.assertEqual(returncode, DUNNO, f"Response is {actioncode_to_string(returncode)}")

    def test_skip(self):
        """Test skip"""
        confdefaults = """
[test]        
limiterfile=/etc/fuglu/ratelimit.yml'
backendtype=memory
backendconfig=
state=rcpt
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
dummyfromrule1:
    strategy: always-hit
    rate: 1/1
    state: rcpt
    action: SKIP    
    count:
        addr:
            type: MilterSession
            ipmatch: 192.168.1.1
dummyfromrule2:
    strategy: always-hit
    rate: 1/1
    state: rcpt
    action: DEFER    
    count:
        addr:
            type: MilterSession
            ipmatch: 192.168.1.1
        from_address:
            type: MilterSession
            regex: ^sender1@domain\.invalid$
""")
        with patch.object(RateLimitPlugin, 'yamlfile2dict', return_value=setupdict):
            rplugin = RateLimitPlugin(config=conf)

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.addr = "192.168.1.1"
        suspect.sender = ''
        suspect.recipients = ['recipient@domain.invalid']

        res = asyncio.run(rplugin.core(suspect=suspect, state=RCPT))
        print(res)
        returncode = res[0] if isinstance(res, tuple) else res
        self.assertEqual(returncode, DUNNO, f"Response is {actioncode_to_string(returncode)}")

        # any suspect will match limiter condition
        suspect.sender = 'sender1@domain.invalid'
        res = asyncio.run(rplugin.core(suspect=suspect, state=RCPT))
        print(res)
        returncode = res[0] if isinstance(res, tuple) else res
        self.assertEqual(returncode, DUNNO, f"Response is {actioncode_to_string(returncode)}")

        # any suspect will match limiter condition
        suspect.addr = "192.168.1.2"
        suspect.sender = 'sender1@domain.invalid'
        res = asyncio.run(rplugin.core(suspect=suspect, state=RCPT))
        print(res)
        returncode = res[0] if isinstance(res, tuple) else res
        self.assertEqual(returncode, DUNNO, f"Response is {actioncode_to_string(returncode)}")


class TestBackendsFull(unittest.TestCase):
    # Base tests for backends

    def test_memory(self):
        """Test different actions, default & bounceaction"""
        confdefaults = """
[test]        
limiterfile=/etc/fuglu/ratelimit.yml'
backendtype=memory
backendconfig=
state=rcpt
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
dummyfromrule2:
    strategy: fixed
    rate: 10/10
    state: rcpt
    action: DEFER    
    count:
        addr:
            type: MilterSession
""")
        with patch.object(RateLimitPlugin, 'yamlfile2dict', return_value=setupdict):
            rplugin = RateLimitPlugin(config=conf, section="test")

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.addr = "192.168.1.1"
        suspect.sender = ''
        suspect.recipients = ['recipient@domain.invalid']

        res = asyncio.run(rplugin.core(suspect=suspect, state=RCPT))
        print(res)
        res = asyncio.run(rplugin.core(suspect=suspect, state=RCPT))
        print(res)
        res = res[0] if isinstance(res, (tuple, list)) else res
        self.assertEqual(res, DUNNO)

    def test_redis(self):
        """redis backend with sliding-window"""
        confdefaults = """
[test]        
limiterfile=/etc/fuglu/ratelimit.yml'
backendtype=redis
backendconfig=redis://redis:6379/1
state=rcpt
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
dummyfromrule2:
    strategy: sliding-window
    rate: 10/10
    state: rcpt
    action: DEFER    
    bounceaction: REJECT
    count:
        addr:
            type: MilterSession
""")
        with patch.object(RateLimitPlugin, 'yamlfile2dict', return_value=setupdict):
            rplugin = RateLimitPlugin(config=conf, section="test")

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.addr = "192.168.1.1"
        suspect.sender = ''
        suspect.recipients = ['recipient@domain.invalid']

        res = asyncio.run(rplugin.core(suspect=suspect, state=RCPT))
        print(res)
        res = asyncio.run(rplugin.core(suspect=suspect, state=RCPT))
        print(res)
        res = res[0] if isinstance(res, (tuple, list)) else res
        self.assertEqual(res, DUNNO)

    def test_aioredis(self):
        """async-redis backend with sliding-window"""
        confdefaults = """
[test]        
limiterfile=/etc/fuglu/ratelimit.yml'
backendtype=aioredis
backendconfig=redis://redis:6379/1
state=rcpt
"""
        conf = ConfigParser()
        conf.read_string(confdefaults)

        setupdict = yaml.safe_load("""
dummyfromrule2:
    strategy: sliding-window
    rate: 10/10
    state: rcpt
    action: DEFER    
    bounceaction: REJECT
    count:
        addr:
            type: MilterSession
""")
        with patch.object(RateLimitPlugin, 'yamlfile2dict', return_value=setupdict):
            rplugin = RateLimitPlugin(config=conf, section="test")

        # create suspect which should match limiter conditions
        suspect = sm.MilterSession(None)
        suspect.addr = "192.168.1.1"
        suspect.sender = ''
        suspect.recipients = ['recipient@domain.invalid']

        try:
            loop = asyncio.get_running_loop()
        except AttributeError:
            # python 3.6
            loop = asyncio.get_event_loop()
        except RuntimeError:
            loop = asyncio.new_event_loop()

        res = loop.run_until_complete(rplugin.core(suspect=suspect, state=RCPT))
        print(res)
        res = loop.run_until_complete(rplugin.core(suspect=suspect, state=RCPT))
        print(res)
        res = res[0] if isinstance(res, (tuple, list)) else res
        self.assertEqual(res, DUNNO)
