# -*- coding: UTF-8 -*-
import unittest
import os
import tempfile
import shutil
from unittestsetup import CONFDIR

from fuglu.plugins.attachment import FiletypePlugin, RulesCache
from fuglu.shared import actioncode_to_string, Suspect, FuConfigParser


class AttachmentFileRegex(unittest.TestCase):
    """Test forward lookup"""
    tempdir = None
    template = None
    candidate = None

    @classmethod
    def setUpClass(cls):
        cls.tempdir = tempfile.mkdtemp('attachtest', 'fuglu')
        cls.template = '%s/blockedfile.tmpl' % cls.tempdir
        shutil.copy(CONFDIR + '/templates/blockedfile.tmpl.dist', cls.template)

        # make sure files are empty except the one with filenames which is used for test here
        with open(f"{cls.tempdir}/default-filetypes.conf", 'w') as f:
            f.write("#empty")
        with open(f"{cls.tempdir}/default-archivefiletypes.conf", 'w') as f:
            f.write("#empty")
        with open(f"{cls.tempdir}/default-archivenames.conf", 'w') as f:
            f.write("#empty")
        with open(f"{cls.tempdir}/default-filenames.conf", 'w') as f:
            f.write(
                r"""# testing...
deny    (?<!^Part)\.[0-9]{3}$    neg part forward lookup
"""
            )
        config = FuConfigParser()
        config.add_section('FiletypePlugin')
        config.set('FiletypePlugin', 'template_blockedfile', cls.template)
        config.set('FiletypePlugin', 'rulesdir', cls.tempdir)
        config.set('FiletypePlugin', 'blockaction', 'DELETE')
        config.set('FiletypePlugin', 'sendbounce', 'False')
        config.set('FiletypePlugin', 'checkarchivenames', 'True')
        config.set('FiletypePlugin', 'checkarchivecontent', 'True')
        config.set('FiletypePlugin', 'archivecontentmaxsize', '7000000')
        config.set('FiletypePlugin', 'archiveextractlevel', -1)
        config.set('FiletypePlugin', 'enabledarchivetypes', 'zip, z, rar, tar, 7z, gz')

        config.add_section('main')
        config.set('main', 'disablebounces', '1')
        config.set('main', 'nobouncefile', '')
        cls.candidate = FiletypePlugin(config)
        cls.rulescache = RulesCache(cls.tempdir)
        cls.candidate.rulescache = cls.rulescache

    @classmethod
    def tearDownClass(cls):
        os.remove(f'{cls.tempdir}/default-filenames.conf')
        os.remove(f'{cls.tempdir}/default-filetypes.conf')
        os.remove(f"{cls.tempdir}/default-archivefiletypes.conf")
        os.remove(f"{cls.tempdir}/default-archivenames.conf")
        os.remove(cls.template)
        shutil.rmtree(cls.tempdir)

    def _prepare_attach_filename(self, filename: str) -> Suspect:
        from email.mime.application import MIMEApplication
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText
        from email.utils import formatdate
        from email.header import Header

        msg = MIMEMultipart()
        msg['From'] = "sender@fuglu.org"
        msg['To'] = "receiver@fuglu.org"
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = "whatever..."

        msg.attach(MIMEText("This is some text..."))

        part = MIMEApplication(b"dummy", Name=filename)
        hdr = Header('attachment', header_name="Content-Disposition", continuation_ws=' ')
        part["Content-Disposition"] = hdr
        msg.attach(part)

        suspect = Suspect("from@fuglu.unittest", "to@fuglu.unittest", "/dev/null")
        suspect.set_message_rep(msg)
        return suspect

    def test_partallow(self):
        # Test blocking of a statically linked executable in archive due to executable rule

        filename = "Part.001"
        suspect = self._prepare_attach_filename(filename)

        result = self.candidate.examine(suspect)
        if type(result) is tuple:
            result, message = result
            print(message)
        resstr = actioncode_to_string(result)
        print(f"filename: {filename} -> result: {resstr}")
        self.assertEqual(resstr, "DUNNO")

    def test_partallow_small(self):
        # test if check is case insensitive

        filename = "part.001"
        suspect = self._prepare_attach_filename(filename)

        result = self.candidate.examine(suspect)
        if type(result) is tuple:
            result, message = result
            print(message)
        resstr = actioncode_to_string(result)
        print(f"filename: {filename} -> result: {resstr}")
        self.assertEqual(resstr, "DUNNO")

    def test_block_before(self):
        # Test blocking of a statically linked executable in archive due to executable rule
        filename = "bla.Part.001"
        suspect = self._prepare_attach_filename(filename)

        result = self.candidate.examine(suspect)
        if type(result) is tuple:
            result, message = result
            print(message)
        resstr = actioncode_to_string(result)
        print(f"filename: {filename} -> result: {resstr}")
        self.assertEqual(resstr, "DELETE")
        self.assertEqual(message, f"{filename}: neg part forward lookup")

    def test_block(self):
        # Test blocking of a statically linked executable in archive due to executable rule

        filename = "Part.01"
        suspect = self._prepare_attach_filename(filename)
        result = self.candidate.examine(suspect)

        if type(result) is tuple:
            result, message = result
            print(message)
        resstr = actioncode_to_string(result)
        print(f"filename: {filename} -> result: {resstr}")
        self.assertEqual(resstr, "DUNNO")
