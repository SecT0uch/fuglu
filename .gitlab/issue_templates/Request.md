> Please post your Plugin- or feature request here, and hopefully one of the fuglu developers or another kind thinks this is a good idea and will implement it!
> Please describe your requested feature in as much details as possible, including the necessary configuration options it should have 
> 
> oh, and also, remove this block before submitting your request, thanks!


/label ~"plugin/feature request"
